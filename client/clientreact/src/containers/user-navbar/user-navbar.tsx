import React, { Component } from 'react';
import LocalStorageService from '../../shared/services/localStorage';
import { Link } from 'react-router-dom';
import AuthService from '../../shared/services/authService';

export default class UserNavbar extends Component<any, any> {

    public authService:AuthService = new AuthService();

    constructor (props: any) {
        super(props);
    }

    public logout() {
        this.authService.logout();
        this.props.props.history.push('/login')
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="/">E-library</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link to='/user'>Home</Link>
                        </li>
                        <li className="nav-item active">
                            <Link to='/user/change-setting'>Change setting</Link>
                        </li>
                    </ul>
                    <button className="btn btn-primary" onClick={() => this.logout()}>Log Out</button>
                </div >
            </nav >
        )
    }

}