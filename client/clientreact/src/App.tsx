import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Login from './components/login';
import Register from './components/register';
import Admin from './components/admin/admin';
import './App.scss';
import User from './components/user/user';
import jwt_decode from 'jwt-decode';


class App extends Component {
  
  public checkUserInfo():any {
    const token: any = localStorage.getItem('token');
    if(token) {
      var decode: any = jwt_decode(token);
    }
    if (!token) {
      return <Switch>
        <Redirect from="/" to="/login" exact />
        <Route path="/login" component={Login}  />
        <Route path="/register" component={Register} exact />
        <Redirect from="*" to="/login" exact />
      </Switch>
    }

    if (token && decode.role === 2) {
      return<Switch>
        <Route path="/admin" component={Admin}  />
        <Redirect to="/admin" exact />
      </Switch>
    }
    if (token && decode.role === 1) {
      return <Switch>
        <Route path="/user" component={User}  />
        <Redirect from="*" to="/user" exact />
      </Switch>
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.checkUserInfo()}
      </React.Fragment>
    );
  }
}

export default App;
