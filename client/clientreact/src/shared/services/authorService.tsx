import axios, { AxiosPromise } from 'axios';
import { environments }  from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IAuthorModel } from '../../../../../shared/models';

export default class AuthorService extends AuthInterceptor {

    public getAuthors(itemsPerPage: number, currentPage: number, searchValue: string):AxiosPromise  {
        const body = {
            itemsPerPage: itemsPerPage,
            currentPage: currentPage,
            searchValue: searchValue
        }
        return axios.post(environments.url + '/authors/paging', body);
    }

    public getAll():AxiosPromise  {
      return axios.get(environments.url + "/author");
    }

    public getById(id: string):AxiosPromise  {
      return axios.get(environments.url + "/author/" + id);
    }

    public add(author: IAuthorModel | any):AxiosPromise  {
      const body = { id: author._id, name: author.name };
      return axios.post(environments.url + '/author', body);
    }

    public update(id: string, author: IAuthorModel | any):AxiosPromise  {
        const body = {
          name: author.name
        };
        return axios.put(environments.url + "/author/" + id, body);
      }

    public delete(authorId: string):AxiosPromise  {
        return axios.delete(environments.url + "/author/" + authorId);
      }

}