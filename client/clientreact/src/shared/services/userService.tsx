import axios, { AxiosPromise } from 'axios';
import { environments }  from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IUserModel } from '../../../../../shared/models';



export default class UserService extends AuthInterceptor {

  public getUser():AxiosPromise {
    return axios.get(environments.url + "/auth/profile")
  }

  public update(id: string, user: IUserModel | any):AxiosPromise {
    const body = {
      fullName: user.fullName,
      email: user.email,
      password: user.password,
      confirm: user.confirm
    }
    return axios.put(environments.url + "/profile/" +id, body);
  }
}