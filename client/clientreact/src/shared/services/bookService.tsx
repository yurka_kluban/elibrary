import axios, { AxiosPromise } from 'axios';
import { environments }  from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IBookModel } from '../../../../../shared/models';

export default class BookService extends AuthInterceptor {

    public getById(bookId: string):AxiosPromise {
        return axios.get(environments.url + "/book/" + bookId);
      }

    public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string):AxiosPromise  {
        const body = {
            itemsPerPage: itemsPerPage,
            currentPage: currentPage,
            searchValue: searchValue
          };
          console.log(body);
          return axios.post(environments.url + '/book/paging', body);
    }

    public getByAuthorId(id: string):AxiosPromise  {
      return axios.get(environments.url + '/book/author/'+id)
    }

    public add(book: IBookModel | any):AxiosPromise  {
      const body = {
        title: book.title,
        image: book.image,
        language: book.language,
        published: book.published,
        pages: book.pages,
        author: book.author,
        journal: book.journal
      }
      return axios.post(environments.url + '/book', body);
    }
  
    public update(bookId: string, book: IBookModel | any):AxiosPromise  {
      const body = {
        title: book.title,
        image: book.image,
        language: book.language,
        published: book.published,
        pages: book.pages,
        author: book.author,
        journal: book.journal
      }
      return axios.put(environments.url + "/book/" + bookId, body);
    }

    public delete(bookId: string):AxiosPromise  {
      return axios.delete(environments.url + "/book/" + bookId);
    }
  
}
