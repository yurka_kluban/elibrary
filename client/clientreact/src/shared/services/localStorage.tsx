import decode from 'jwt-decode';
import AuthInterceptor from '../interceptors/authInterceptor';


export default class LocalStorageService extends AuthInterceptor {

    setToken(token: string): void {
        localStorage.setItem('token', token)
    }

    getToken() {
        return localStorage.getItem('token')
    }

    remove(item: string): void{
        localStorage.removeItem('token');
    }
}