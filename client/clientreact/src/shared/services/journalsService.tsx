import axios, { AxiosPromise } from 'axios';
import { environments } from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IJournalModel } from '../../../../../shared/models';

export default class JournalsService extends AuthInterceptor {

    public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string):AxiosPromise  {
        const body = {
            itemsPerPage: itemsPerPage,
            currentPage: currentPage,
            searchValue: searchValue
        };
        return axios.post(environments.url + '/journals/paging', body);
    }
    
    public getAll():AxiosPromise  {
        return axios.get(environments.url + "/journal");
    }

    public add(journal: IJournalModel | any):AxiosPromise  {
        const body = { id: journal._id, title: journal.title };
        return axios.post(environments.url + '/journal', body);
    }

    public update(_id: string, journal: IJournalModel | any):AxiosPromise  {
        const body = {
            title: journal.title
        }
        return axios.put(environments.url + "/journal/" +_id, body);
    }

    public delete(journalId: string):AxiosPromise  {
        return axios.delete(environments.url + "/journal/" + journalId);
    }

}