
import axios, { AxiosPromise } from 'axios';
import { environments }  from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IUserModel } from '../../../../../shared/models';
import LocalStorageService from './localStorage';



export default class AuthService extends AuthInterceptor {

    public localStorageService:LocalStorageService = new LocalStorageService();

    public login(user: IUserModel | any):AxiosPromise  {
        const body = {
            email: user.email,
            password: user.password,
            confirm: user.confirm
        };
        return axios.post(environments.url + '/auth/login', body);
    }

    public register(user: IUserModel | any):AxiosPromise  {
        const body = {
            fullName: user.fullName,
            email: user.email,
            password: user.password,
            confirm: user.confirm
        };
        return axios.post(environments.url + '/auth/register', body);
    }

    public logout() {
        this.localStorageService.remove('token');
    }

}