import axios, { AxiosPromise } from 'axios';
import { environments }  from '../../environments';
import AuthInterceptor from '../interceptors/authInterceptor';
import { IUserModel } from '../../../../../shared/models';


export default class AdminService extends AuthInterceptor {

    public get(): AxiosPromise {
      return axios.get(environments.url + "/admin");
    }

    public getUnconfirmUsers():AxiosPromise {
        return axios.get(environments.url + '/admin/unconfirmUsers');
    }

    public getAllUsersForPagination(itemsPerPage: number, currentPage: number, searchValue: string) {
        const body = {
          itemsPerPage: itemsPerPage,
          currentPage: currentPage,
          searchValue: searchValue
        };
        return axios.post(environments.url + '/admin/users/paging', body);
    }

    public add(user: IUserModel | any):AxiosPromise  {
      const body = {
          fullName: user.fullName,
          email: user.email,
          password: user.password,
          confirm: user.confirm,
          role: user.role
        };
    return axios.post(environments.url + '/admin', body);
}

      public ban(id: string):AxiosPromise  {
        const body = { confirm: false }
        return axios.put(environments.url + "/admin/confirmOrBan/" + id, body);
    }  

      public confirm(id: string):AxiosPromise  {
        const body = { confirm: true }
        return axios.put(environments.url + "/admin/confirmOrBan/" + id, body);
    }

    public update(id: string, user: IUserModel | any):AxiosPromise  {
      const body = {
          fullName: user.fullName,
          email: user.email,
          password: user.password,
          confirm: user.confirm,
          role: user.role
      }
      return axios.put(environments.url + "/admin/" + id, body);
  }

      public delete(id: string):AxiosPromise  {
        return axios.delete(environments.url + "/admin/" + id);
    }
}