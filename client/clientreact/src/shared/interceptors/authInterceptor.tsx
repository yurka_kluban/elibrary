import axios from 'axios';

export default class AuthInterceptor {
    constructor () {
        const token = localStorage.getItem('token');
        if (token) {
            axios.defaults.headers.common = {'x-access-token': token} 
        }
        if(!token) {
            console.log('NOT TOKEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        }
    }
}