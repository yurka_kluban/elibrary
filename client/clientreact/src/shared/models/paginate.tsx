export interface Paginate {
    lengthArray: number,
    currentPage: number,
    itemsPerPage: number
}