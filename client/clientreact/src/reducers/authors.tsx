export default function(state = [], action: any){
    switch (action.type) {
        case "GET_AUTHORS":
            return action.payload
        
        default:
            return state;
    }
}