import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';
import booksByAuthorId  from './getBooksByAuthorId';
import unconfirmUsers from './unconfirmUsers';
import confirmUsers from './confirmUsers';
import authors from './authors';
import author from './getAuthorById';
import journals from './journals';
import books from './booksPaginate';
import book from './getBookById';
import paginate from './paginate';
import auth from './auth';
import userInfo from './userInfo';





const allReducers = combineReducers({
    confirmUsers: confirmUsers,
    unconfirmUsers: unconfirmUsers,
    booksByAuthorId: booksByAuthorId,
    userInfo: userInfo,
    paginate: paginate,
    authors: authors,
    author:author,
    journals: journals,
    books: books,
    book: book,
    auth: auth,
    router: routerReducer
});

export default allReducers;