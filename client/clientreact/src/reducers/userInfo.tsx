export default function(state = [], action: any){
    switch (action.type) {
        case "USER_INFO":
            return action.payload
        
        default:
            return state;
    }
}