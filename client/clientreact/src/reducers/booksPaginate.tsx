export default function(state = [], action: any){
    switch (action.type) {
        case "BOOKS_PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}