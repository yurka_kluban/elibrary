const booksByAuthorId = {
    journal: [],
    author: [],
}

export default function(state = booksByAuthorId, action: any){
    switch (action.type) {
        case "BOOKS_BY_AUTHOR_ID":
            return action.payload
        
        default:
            return state;
    }
}