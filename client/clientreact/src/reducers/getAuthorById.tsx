export default function(state = {}, action: any){
    switch (action.type) {
        case "AUTHOR_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}