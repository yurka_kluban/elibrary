import { Paginate } from '../shared/models';

const pagination = {
    lengthArray: 0,
    currentPage: 1,
    itemsPerPage: 2,
}

export default function(state:Paginate = pagination, action: any){
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}