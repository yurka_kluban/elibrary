export default function(state = [], action: any){
    switch (action.type) {
        case "UNCONFIRM_USERS":
            return action.payload
        
        default:
            return state;
    }
}