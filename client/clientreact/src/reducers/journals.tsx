export default function(state = [], action: any){
    switch (action.type) {
        case "GET_JOURNALS":
            return action.payload
        
        default:
            return state;
    }
}