const book = {
    author: [],
}

export default function(state = book, action: any){
    switch (action.type) {
        case "BOOK_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}