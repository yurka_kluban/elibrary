export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILURE = 'AUTH_FAILURE';

export const login = (email: string, password: string) => ({
    type: AUTH_REQUEST,
    payload: { email, password }
});

const initialState = {
    token: localStorage.getItem('token'),
    error: null
};

export default function(state = initialState, action:any) {
    switch (action.type) {
        case AUTH_SUCCESS: {
            return { ...state, token: action.payload };
        }
        case AUTH_FAILURE: {
            return { ...state, error: action.payload }
        }
        default:
            return state;
    }
};