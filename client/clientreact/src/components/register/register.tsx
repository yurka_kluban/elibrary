import React, { Component } from 'react';
import './register.scss'
import { Link } from 'react-router-dom';
import AuthService from '../../shared/services/authService';

//  interface ReactPropTypes extends React.Props<Register> {
//     history: any
//   }
export default class Register extends Component<any, any> {

  public authService:AuthService = new AuthService()

  constructor(props: any) {
    super(props);
    this.state = {
        fullName: "",
        email: "",
        password: "",
        confirm: false
      };
  }

  public onChange = (event: { target: { name: string; value: string; }; }) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  public onSubmit = (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    const newUser = {
      fullName: this.state.fullName,
      email: this.state.email,
      password: this.state.password,
      confirm: this.state.confirm
    };
    this.authService.register(newUser).then(() => {
      this.props.history.push('/login');
    })
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <h2>Sign Up!</h2>
        <div className="img">
        </div>
        <div className="form-group">
          <label htmlFor="Full Name">Full Name</label>
          <input type="text" className="form-control" 
          onChange={this.onChange}
          value={this.state.fullName} 
          name="fullName"
          placeholder="Enter your Full Name" />
        </div>
        <div className="form-group">
          <label htmlFor="Email">Email address</label>
          <input type="email" className="form-control" 
            onChange={this.onChange}
            value={this.state.email}
            name="email"
            id="exampleInputEmail1" 
            aria-describedby="emailHelp"
            placeholder="Enter your E-mail" required pattern="[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}" />
        </div>
        <div className="form-group">
          <label htmlFor="Password">Password</label>
          <input type="password" className="form-control" 
          onChange={this.onChange}
          value={this.state.password}
          name="password"
          placeholder="Password" />
        </div>
        <button type="submit" className="btn btn-primary">Registration</button>
        <Link to='/login'>Login</Link>
      </form>
    )
  }
}