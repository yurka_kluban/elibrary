import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import UserHome from './user-home/user-home'
import './user.scss'
import UserNavbar from '../../containers/user-navbar';
import ChangeSetting from './change-privacy-setting/change-setting';
import ShowBook from './user-home/show-book/show-book';
import ShowAuthor from './user-home/show-author/show-author';

export default class User extends Component {

  render() {
    return (
      <div>
        <UserNavbar props={this.props} />
        <Switch>
          <Route path="/user" component={UserHome} exact />
          <Route path="/user/change-setting" component={ChangeSetting} exact />
          <Route path="/user/show-book" component={ShowBook} />
          <Route path="/user/show-author" component={ShowAuthor} />
        </Switch>
      </div>
    )
  }
}