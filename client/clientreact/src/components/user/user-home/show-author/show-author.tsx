import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import BookService from '../../../../shared/services/bookService';
import AuthorService from '../../../../shared/services/authorService';
import { Link } from 'react-router-dom';
import { IAuthorModel, IBookModel } from '../../../../../../../shared/models';


export const getAuthorByIdAction = (author: IAuthorModel) => {
    return {
        type: "AUTHOR_BY_ID",
        payload: author
    }
}

export const getAuthorById = (state: IAuthorModel, action: { type: string; payload: IAuthorModel; }) => {
    switch (action.type) {
        case "AUTHOR_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}

export const getBooksByAuthorIdAction = (booksByauthorId:IBookModel) => {
    return {
        type: "BOOKS_BY_AUTHOR_ID",
        payload: booksByauthorId
    }
}

export const getBooksByAuthorId = (state:IBookModel, action: { type: string; payload: IBookModel; } ) => {
    switch (action.type) {
        case "BOOKS_BY_AUTHOR_ID":
            return action.payload
        
        default:
            return state;
    }
}

class ShowAuthor extends Component<any, any> {

    public bookService:BookService = new BookService();
    public authorService:AuthorService = new AuthorService();

    public id: string = '';
    public author: IAuthorModel[] = [];
    public books: IBookModel[] = [];

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {
        this.getAuthor();
        this.getBooksByAuthorId()
    }

    public getAuthor(): void {
        // this.id = this.props.location.query.bookId;
        this.id = location.pathname.split('/')[3];
        this.authorService.getById(this.id).then(res => {
            this.props.getAuthorByIdAction(res.data as IAuthorModel[]);
        })
    }

    public getBooksByAuthorId(): void {
        this.bookService.getByAuthorId(this.id).then(res => {
            this.props.getBooksByAuthorIdAction(res.data[0] as IBookModel[]);
        });
    }

    public authorDetails() {
        return (
            <React.Fragment>
                <ul className="list-group">
                    <li className="list-group-item list-group-item-light">
                        Author: {this.props.author.name}
                    </li>
                </ul>
                <ul> 
                    <li className="list-group-item list-group-item-light">
                            <Link to={'/user/show-book/' + this.props.booksByAuthorId._id}> {this.props.booksByAuthorId.title}</Link>
                    </li>
                </ul>
            </React.Fragment>
        )
    }

    render() {
        return (
            <div className="media">
                <div className="media-body">
                    {this.authorDetails()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: { author: IAuthorModel; booksByAuthorId: IBookModel; }) {
    console.log(state)
    return {
        author: state.author,
        booksByAuthorId: state.booksByAuthorId
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({ 
        getAuthorByIdAction: getAuthorByIdAction,
        getBooksByAuthorIdAction: getBooksByAuthorIdAction
     }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(ShowAuthor);

