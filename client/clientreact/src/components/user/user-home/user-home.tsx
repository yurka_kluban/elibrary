import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import BookService from '../../../shared/services/bookService';
import AuthorService from '../../../shared/services/authorService';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import { IBookModel, IAuthorModel, IJournalModel } from '../../../../../../shared/models';
import { Paginate } from '../../../shared/models';

export const paginateAction = (usersPaginate: Paginate) => {
    return {
        type: "PAGINATE",
        payload: usersPaginate
    }
}

export const paginate = (state:Paginate, action: { type: string; payload: Paginate; }) => {
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

export const authorsAction = (authors: IAuthorModel[]) => {
    return {
        type: "GET_AUTHORS",
        payload: authors
    }
}

export const authors = (state:IAuthorModel[], action: { type: string; payload: IAuthorModel; }) => {
    switch (action.type) {
        case "GET_AUTHORS":
            return action.payload
        
        default:
            return state;
    }
}

export const booksPaginateAction = (books: IBookModel) => {
    return {
        type: "BOOKS_PAGINATE",
        payload: books
    }
}

export const booksPaginate = (state: IBookModel[], action: { type: string; payload: IBookModel; }) => {
    switch (action.type) {
        case "BOOKS_PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

class UserHome extends Component<any, any> {

    public authorService:AuthorService = new AuthorService();
    public bookService:BookService = new BookService();
    
    public lengthBooksArray: number | undefined;
    public currentPage:number = 1;
    public itemsPerPage:number = 2;
    public timer: any;

    constructor(props: any) {
        super(props);
        this.state = {
            searchValue: '',
        }
    }

    componentWillMount() {
        this.getBooks();
        this.getAuthors();
    }

    public getBooks(): void {
        this.bookService.getAllForPagination(this.itemsPerPage, this.currentPage, this.state.searchValue).then(res => {
            this.lengthBooksArray = res.data.length[0].length;
            this.props.booksPaginateAction(res.data.books as IBookModel)
            const paging = {
                itemsPerPage: this.itemsPerPage,
                currentPage: this.currentPage,
                lengthBooksArray: this.lengthBooksArray,
                searchValue: this.state.searchValue
            }
            this.props.paginateAction(paging);
        })
    }
    
    
    public getAuthors(): void {
        this.authorService.getAll().then(res => {
            this.props.authorsAction(res.data as IAuthorModel[]);
        })
    }

    public getAllBooksForPaging(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.getBooks();
        }, 500);
    }

    public handlePageClick(data: { selected: number; }): void {
        this.currentPage = data.selected + 1;
        this.getAllBooksForPaging();
    }

    public updateInputValue(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            searchValue: event.target.value
        });
        this.getAllBooksForPaging();
    }

    public books() {
        return (
            <React.Fragment>{this.props.books.map((item: IBookModel[] | any, index: number) => {
                return (
                    <div className="col-3" key={index + 1}>
                        <div className="card" >
                            <img src={item.image} alt="Card image cap" className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">Title: <Link key={index} to={{ pathname: '/user/show-book/' + item._id, query: { bookId: item._id } }}> {item.title}</Link></h5>
                                <p className="card-text" key={index + 1}>Author: {item.author.map((item: IAuthorModel, index: number) => {
                                    return (
                                        <Link key={index} to={'/user/show-author/' + item._id}> {item.name}</Link>
                                    )
                                })}</p>
                            </div>
                        </div>
                    </div>
                )
            })
            }
            </React.Fragment>
        )
    }
    render() {
        return (
            <div>
                <div className="active-cyan-3 active-cyan-4 mb-4">
                    <input className="form-control" 
                    type="text" 
                    placeholder="Search" 
                    value={this.state.searchValue} 
                    onChange={event => this.updateInputValue(event)} />
                </div>
                <div className="container-fluid">
                    <div className="row">
                        {this.books()}
                    </div>
                </div>
                <ReactPaginate
                    previousLabel={'prev'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    onPageChange={this.handlePageClick}
                    pageCount={Math.ceil(this.props.paginate.lengthBooksArray / this.props.paginate.itemsPerPage)}
                    marginPagesDisplayed={this.props.currentPage}
                    pageRangeDisplayed={2}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                ></ReactPaginate></div>
        )
    }
}

function mapStateToProps(state: { authors: IAuthorModel; journals: IJournalModel; books: IBookModel; paginate: Paginate; }) {
    return {
        authors: state.authors,
        journals: state.journals,
        books: state.books,
        paginate: state.paginate
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        booksPaginateAction: booksPaginateAction,
        paginateAction: paginateAction,
        authorsAction: authorsAction,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserHome);
