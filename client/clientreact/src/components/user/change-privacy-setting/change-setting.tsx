import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import UserService from '../../../shared/services/userService';
import { IUserModel } from '../../../../../../shared/models';

export const userInfoAction = (userInfo: IUserModel[]) => {
    return {
        type: "USER_INFO",
        payload: userInfo
    }
}

export const userInfo = (state: IUserModel, action: { type: string; payload: IUserModel; }) => {
    switch (action.type) {
        case "USER_INFO":
            return action.payload
        
        default:
            return state;
    }
}

class ChangeSetting extends Component<any, any> {

    public userInfo: IUserModel[] = [];
    public userService:UserService = new UserService();

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {
        this.getUser();
        this.change();

    }

    public changeSettingForm = {
        fullName: '',
        email: '',
        password: ''
    }

    public getUser(): void {
        this.userService.getUser().then((res: any) => {
            this.props.userInfoAction(res.data)
        })
    }
    
    public change(): void {
        this.userService.update(this.props.userInfo.id, this.changeSettingForm).then(() => {
        })
    }

    public onChange = (event: { target: { name: string; value: string; }; }) => {
        this.setState({ [event.target.name]: event.target.value });
      };

    render() {
        return (
            <form onSubmit={this.change}>
                <h2>Change you privacy setting</h2>
                <div className="form-group">
                    <input type="text" 
                    defaultValue={this.props.userInfo.fullName} 
                    onChange={this.onChange} 
                    className="form-control" 
                    name="fullName"  />
                </div>
                <div className="form-group">
                    <input type="email" 
                    defaultValue={this.props.userInfo.email} 
                    onChange={this.onChange} 
                    className="form-control" 
                    id="exampleInputEmail1" 
                    aria-describedby="emailHelp" 
                    name="email"
                    required pattern="[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}" />
                </div>
                <div className="form-group">
                    <input type="password" 
                    defaultValue="password" 
                    onChange={this.onChange} 
                    className="form-control" 
                    id="exampleInputPassword1" 
                    name="password"  />
                </div>
                <button type="submit" className="btn btn-success">Update</button>
                <button type="button" className="btn btn-primary">Cancel</button>
            </form>
        )
    }
}

function mapStateToProps(state: { userInfo: IUserModel; }) {
    return {
        userInfo: state.userInfo
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        userInfoAction: userInfoAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeSetting);

