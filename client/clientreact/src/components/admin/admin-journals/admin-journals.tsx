import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import ReactPaginate from 'react-paginate';
import JournalsService from '../../../shared/services/journalsService'
import './admin-journals.scss';
import ReactModal from 'react-modal';
import { Paginate } from '../../../shared/models';
import { IJournalModel } from '../../../../../../shared/models';

export const paginateAction = (usersPaginate: Paginate) => {
    return {
        type: "PAGINATE",
        payload: usersPaginate
    }
}

export const paginate = (state:Paginate, action: { type: string; payload: Paginate; }) => {
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

export const journalsAction = (journals: IJournalModel[]) => {
    return {
        type: "GET_JOURNALS",
        payload: journals
    }
}

export const journals = (state: IJournalModel, action: { type: string; payload: IJournalModel; }) => {
    switch (action.type) {
        case "GET_JOURNALS":
            return action.payload
        
        default:
            return state;
    }
}

class AdminJournals extends Component<any, any> {

    public journalsService:JournalsService = new JournalsService();

    public statusMessage: string = '';
    public lengthJournalsArray: number | undefined;
    public currentPage:number = 1;
    public itemsPerPage:number = 2;
    public timer: any;

    constructor(props: any) {
        super(props);
        this.state = {
            searchValue: '',
            showModal: false,
            _id: '',
            title: ''
        };
    }

    componentWillMount() {
        ReactModal.setAppElement('body');
        this.getJournals();
    }

    public modalState(): boolean {
        const stateRef = this.state;
        return stateRef["showModal"];
    }

    public getJournals(): void {
        this.journalsService.getAllForPagination(this.itemsPerPage, this.currentPage, this.state.searchValue).then(res => {
            this.props.journalsAction(res.data.journals as IJournalModel[]);
            this.lengthJournalsArray = res.data.length[0].length;
            const paging = {
                itemsPerPage: this.itemsPerPage,
                currentPage: this.currentPage,
                lengthJournalsArray: this.lengthJournalsArray,
                searchValue: this.state.searchValue
            }
            this.props.paginateAction(paging);
        })
    }

    public getAllJournalsForPaging(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.getJournals();
        }, 500);
    }

    public handlePageClick(data: { selected: number; }): void {
        this.currentPage = data.selected + 1;
        this.getAllJournalsForPaging();
    }

    public updateInputValue(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            searchValue: event.target.value
        });
        this.getAllJournalsForPaging();
    }

    public onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    public submit = (event: { preventDefault: () => void; }) => {
        event.preventDefault();
        const journalData = {
            title: this.state.title
        };
        if (this.state._id === "") {
            this.journalsService.add(journalData).then(() => {
                this.getAllJournalsForPaging();
            });
        } else {
            this.journalsService.update(this.state._id, journalData).then(() => {
                this.getAllJournalsForPaging();
            });
        }
    }

    public delete(event: any):void {
        this.journalsService.delete(event.target.id).then(() => {
            this.statusMessage = 'Data successfully deleted';
            this.getAllJournalsForPaging();
        })
    }

    public journalsList() {
        return [].map.call(this.props.journals, (item: IJournalModel, index: number) => {
            return (
                <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>
                        {item.title}
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                    <td>
                        <button type="button" className="btn btn-warning" id={item._id} onClick={() => this.setState({
                            showModal: true, _id: item._id, title: item.title
                        })}>Edit</button>
                    </td>
                    <td>
                        <button type="button" className="btn btn-danger" id={item._id} onClick={() => {
                            this.delete(event)
                        }}>Delete</button>
                    </td>
                </tr>
            )
        });
    }

    render() {
        return (
            <div className="container">
                <div className="reglist">
                    <div className="active-cyan-3 active-cyan-4 mb-4">
                        <input value={this.state.searchValue} 
                        onChange={event => this.updateInputValue(event)} 
                        className="form-control" 
                        type="text" 
                        placeholder="Search" />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={() => this.setState({
                        showModal: true
                    })}>Add Journals</button>
                    <ReactModal
                        isOpen={this.modalState()}
                        onRequestClose={() => this.setState({ showModal: false })}
                        contentLabel="Minimal Modal Example">
                        <form onSubmit={this.submit}>
                            <div className="form-group">
                                <input name="title"
                                    onChange={this.onChange}
                                    defaultValue={this.state.title}
                                    type="text" className="form-control"
                                    placeholder="Enter new title" />
                            </div>
                            <button type="submit" className="btn btn-success">Save</button>
                        </form>
                        <button onClick={() => this.setState({ 
                            showModal: false,
                            _id: '',
                            title: ''
                        })}>Close Modal</button>
                    </ReactModal>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Something</th>
                                <th colSpan={3}></th>

                            </tr>
                        </thead>
                        <tbody>
                            {this.journalsList()}
                        </tbody>
                    </table>
                    <ReactPaginate
                        previousLabel={'prev'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        onPageChange={this.handlePageClick.bind(this)}
                        pageCount={Math.ceil(this.props.paginate.lengthJournalsArray / this.props.paginate.itemsPerPage)}
                        marginPagesDisplayed={this.props.currentPage}
                        pageRangeDisplayed={2}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                    ></ReactPaginate>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: { journals: IJournalModel[]; paginate: Paginate; }) {
    return {
        journals: state.journals,
        paginate: state.paginate
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        journalsAction: journalsAction,
        paginateAction: paginateAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminJournals);