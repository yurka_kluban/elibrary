import React, { Component } from 'react';
import AuthorService from '../../../shared/services/authorService';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import ReactPaginate from 'react-paginate';
import ReactModal from 'react-modal';
import { IAuthorModel } from '../../../../../../shared/models/author.model';
import { Paginate } from '../../../shared/models';
import './admin-authors.scss';


export const paginateAction = (usersPaginate: Paginate) => {
    return {
        type: "PAGINATE",
        payload: usersPaginate
    }
}

export const paginate = (state:Paginate, action: { type: string; payload: Paginate; }) => {
    // type set to default by typescript
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

export const authorsAction = (authors: IAuthorModel) => {
    return {
        type: "GET_AUTHORS",
        payload: authors
    }
}

export const authors = (state: IAuthorModel, action: { type: string; payload: IAuthorModel; }) => {
    switch (action.type) {
        case "GET_AUTHORS":
            return action.payload
        
        default:
            return state;
    }
}

class AdminAuthors extends Component<any, any> {

    public authorService:AuthorService = new AuthorService();

    public statusMessage: string = '';
    public lengthAuthorsArray: number | undefined;
    public currentPage:number = 1;
    public itemsPerPage:number = 2;
    public timer: any;

    constructor(props: any) {
        super(props);
        this.state = {
            searchValue: '',
            showModal: false,
            _id: '',
            name: ''
        };
    }

    componentWillMount() {
        ReactModal.setAppElement('body');
        this.getAuthors();
    }

    public onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ [event.target.name]: event.target.value });
    };

    public submit = (event: React.FormEvent<EventTarget>): void => {
        event.preventDefault();
        const authorData = {
            name: this.state.name
        };
        if (this.state._id === "") {
            this.authorService.add(authorData).then(() => {
                this.getAllAuthorsForPaging();
            })
        } else {
            this.authorService.update(this.state._id, authorData).then(() => {
                this.getAllAuthorsForPaging();
            })
        }
    };

    public modalState(): boolean {
        const stateRef = this.state;
        return stateRef["showModal"];
    }

    public getAuthors(): void {
        this.authorService.getAuthors(this.itemsPerPage, this.currentPage, this.state.searchValue).then(res => {
            this.props.authorsAction(res.data.authors as IAuthorModel[]);
            this.lengthAuthorsArray = res.data.length[0].length;
            const paging = {
                itemsPerPage: this.itemsPerPage,
                currentPage: this.currentPage,
                lengthAuthorsArray: this.lengthAuthorsArray,
                searchValue: this.state.searchValue
            }
            this.props.paginateAction(paging);
        })
    }

    public getAllAuthorsForPaging(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.getAuthors();
        }, 500);
    }

    public handlePageClick(data: number | any): void {
        this.currentPage = data.selected + 1;
        this.getAllAuthorsForPaging();
    }

    public updateInputValue(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            searchValue: event.target.value
        });
        this.getAllAuthorsForPaging();
    }

    public delete(event: any): void {
        this.authorService.delete(event.target.id).then(res => {
            console.log(res);
            this.statusMessage = 'Data successfully deleted';
            this.getAllAuthorsForPaging();
        })
    }

    public authorsTable() {
        return [].map.call(this.props.authors, (item: IAuthorModel, index: number) => {
            return (
                <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>
                        {item.name}
                    </td>
                    <td>

                    </td>
                    <td>
                        <button type="button" className="btn btn-warning" id={item._id} onClick={() => this.setState({
                            showModal: true, _id: item._id, name: item.name
                        })}>Edit</button>
                    </td>
                    <td>
                        <button type="button" className="btn btn-danger" id={item._id} onClick={() => {
                            this.delete(event)
                        }}>Delete</button>
                    </td>
                </tr>
            )
        });
    }

    render() {
        return (
            <div className="container">
                <div className="reglist">
                    <div className="active-cyan-3 active-cyan-4 mb-4">
                        <input value={this.state.searchValue} onChange={event => this.updateInputValue(event)} className="form-control" type="text" placeholder="Search" />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={() => this.setState({
                        showModal: true
                    })}>Add Author</button>
                    <ReactModal
                        isOpen={this.modalState()}
                        onRequestClose={() => this.setState({ showModal: false })}
                        contentLabel="Minimal Modal Example">
                        <form onSubmit={this.submit}>
                            <div className="form-group">
                                <input
                                    name="name"
                                    onChange={this.onChange}
                                    defaultValue={this.state.name}
                                    type="text" className="form-control"
                                    placeholder="Enter new name" />
                            </div>
                            <button type="submit" className="btn btn-success">Save</button>
                        </form>
                        <button onClick={() => this.setState({
                            showModal: false,
                            name: '',
                            _id: ''
                        })}>Close Modal</button>
                    </ReactModal>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Something</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.authorsTable()}
                        </tbody>
                    </table>
                    <ReactPaginate
                        previousLabel={'prev'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        onPageChange={this.handlePageClick.bind(this)}
                        pageCount={Math.ceil(this.props.paginate.lengthAuthorsArray / this.props.paginate.itemsPerPage)}
                        marginPagesDisplayed={this.props.currentPage}
                        pageRangeDisplayed={2}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                    ></ReactPaginate>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: { authors: IAuthorModel; paginate: Paginate; }) {
    return {
        authors: state.authors,
        paginate: state.paginate
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        authorsAction: authorsAction,
        paginateAction: paginateAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminAuthors);