import React, {Component} from 'react';
import AdminNavbar from '../../containers/admin-navbar';
import { Switch, Route } from 'react-router-dom';
import './admin.scss';
import AdminBooks from './admin-books';
import AdminAuthors from './admin-authors';
import AdminJournals from './admin-journals';
import AdminUsers from './admin-users';
import AdminHome from './admin-home';
import ShowBook from './admin-books/show-book/show-book';
import ShowAuthor from './admin-books/show-author/show-author';


export default class Admin extends Component {
  render() {    
    return (
        <div>
          < AdminNavbar props={this.props} />
          <Switch>
            <Route path="/admin" component={AdminHome} exact />
            <Route path="/admin/admin-books" component={AdminBooks} exact />
            <Route path="/admin/admin-books/show-book" component={ShowBook} />
            <Route path="/admin/admin-books/show-author" component={ShowAuthor} />
            <Route path="/admin/admin-authors" component={AdminAuthors}  />
            <Route path="/admin/admin-journals" component={AdminJournals}  />
            <Route path="/admin/admin-users" component={AdminUsers}  />
          </Switch>
        </div>
    )
  }
}