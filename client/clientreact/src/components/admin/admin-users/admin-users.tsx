import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import AdminService from '../../../shared/services/adminService';
import './admin-users.scss';
import ReactPaginate from 'react-paginate';
import ReactModal from 'react-modal';
import { Paginate } from '../../../shared/models';
import { IUserModel } from '../../../../../../shared/models';

export const confirmUsersAction = (confirmUsers: IUserModel[]) => {
    return {
        type: "CONFIRM_USERS",
        payload: confirmUsers
    }
}

export const confirm = (state: IUserModel, action: { type: string; payload: IUserModel; }) => {
    switch (action.type) {
        case "CONFIRM_USERS":
            return action.payload
        
        default:
            return state;
    }
}

export const paginateAction = (usersPaginate: Paginate) => {
    return {
        type: "PAGINATE",
        payload: usersPaginate
    }
}

export const paginate = (state:Paginate, action: { type: string; payload: Paginate; }) => {
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

class AdminUsers extends Component<any, any> {

    public adminService:AdminService = new AdminService();

    constructor(props: any) {
        super(props);
        this.state = {
            fullName: '',
            email: '',
            password: '',
            role: '',
            confirm: true,
            searchValue: '',
            showModal: false,
            _id: ''
        };
    }

    public statusMessage: string = '';
    public lengthUsersArray: number | undefined;
    public currentPage:number = 1;
    public itemsPerPage:number = 2;
    public timer: any;
    public adminInfo: any;


    componentWillMount() {
        ReactModal.setAppElement('body');
        this.getUsers();
        this.getPageId();
    }

    public modalState(): boolean {
        const stateRef = this.state;
        return stateRef["showModal"];
    }

    public getPageId() {
        this.adminService.get().then((res: any) => {
          this.adminInfo = res.data;
          console.log(this.adminInfo)
        });
      }

    public getUsers(): void {
        this.adminService.getAllUsersForPagination(this.itemsPerPage, this.currentPage, this.state.searchValue).then(res => {
            this.props.confirmUsersAction(res.data.users as IUserModel[]);
            this.lengthUsersArray = res.data.length[0].length;
            const paging = {
                itemsPerPage: this.itemsPerPage,
                currentPage: this.currentPage,
                lengthUsersArray: this.lengthUsersArray,
                searchValue: this.state.searchValue
            }
            this.props.paginateAction(paging);
        })
    }

    public getAllUsersForPaging(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.getUsers();
        }, 500);
    }

    public handlePageClick(data: { selected: number; }): void {
        this.currentPage = data.selected + 1;
        this.getAllUsersForPaging();
    }

    public updateInputValue(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            searchValue: event.target.value
        });
        this.getAllUsersForPaging();
    }

    public onChange = (event: { target: { name: string; value: string; }; }) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    public ban(event: any): void {
        this.adminService.ban(event.target.id).then(() => {
            this.getAllUsersForPaging();
        })
    }

    public submit = (event: { preventDefault: () => void; }) => {
        event.preventDefault();
        const user = {
            fullName: this.state.fullName,
            email: this.state.email,
            password: this.state.password,
            role: this.state.role,
            confirm: this.state.confirm
        }
        if (this.state._id === "") {
            this.adminService.add(user).then(() => {
                this.getAllUsersForPaging();
            })
        } else {
            this.adminService.update(this.state._id, user).then(() => {
                this.getAllUsersForPaging();
            })
        }
    }

    public delete(event: any): void {
        if (this.adminInfo.id == event.target.id){
            alert('YOU CANT DELETE OURSELF!!!!')
            return;
          }
        this.adminService.delete(event.target.id).then(() => {
            this.statusMessage = 'Data successfully deleted';
            this.getAllUsersForPaging();
        })
    }

    public confirmUsersList() {
        return [].map.call(this.props.confirmUsers, (item: IUserModel, index: number) => {
            return (
                <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>
                        {item.fullName}
                    </td>
                    <td>
                        {item.email}
                    </td>
                    <td>
                        {item.role}
                    </td>
                    <td>
                        <button type="button" className="btn btn-warning" id={item._id} onClick={() => this.setState({
                            showModal: true, _id: item._id, fullName: item.fullName, email: item.email, password: item.password, role: item.role
                        })}>Edit</button>
                    </td>
                    <td>
                        <button type="button" className="btn btn-info" id={item._id} onClick={() => {
                            this.ban(event)
                        }}>Ban</button>
                    </td>
                    <td>
                        <button type="button" className="btn btn-danger" id={item._id} onClick={() => {
                            this.delete(event)
                        }}>Delete</button>
                    </td>
                </tr>
            )
        });
    }

    render() {
        return (
            <div className="container">
                <div className="reglist">
                    <div className="active-cyan-3 active-cyan-4 mb-4">
                        <input className="form-control" type="text" placeholder="Search" 
                        value={this.state.searchValue} 
                        onChange={event => this.updateInputValue(event)} />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={() => this.setState({
                        showModal: true
                    })}>Add User</button>
                    <ReactModal
                        isOpen={this.modalState()}
                        onRequestClose={() => this.setState({ showModal: false })}
                        contentLabel="Minimal Modal Example">
                        <form onSubmit={this.submit}>
                            <div className="form-group">
                                <input name="fullName"
                                    defaultValue={this.state.fullName}
                                    onChange={this.onChange}
                                    type="text" className="form-control" />
                            </div>
                            <div className="form-group">
                                <input name="email"
                                    defaultValue={this.state.email}
                                    onChange={this.onChange}
                                    type="email" className="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter your E-mail" required pattern="[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}" />
                            </div>
                            <div className="form-group">
                                <input name="password"
                                    onChange={this.onChange}
                                    defaultValue={this.state.password}
                                    type="password" className="form-control"
                                    id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            <div className="form-group">
                                <div className="input-group mb-3">
                                    <select className="custom-select"
                                        id="inputGroupSelect02"
                                        name="role"
                                        defaultValue={String(this.state.role)}
                                        onChange={this.onChange}>
                                        <option value="1">User</option>
                                        <option value="2">Admin</option>
                                    </select>
                                    <div className="input-group-append">
                                        <label className="input-group-text" htmlFor="inputGroupSelect02">Roles</label>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-success">Save</button>
                        </form>
                        <button onClick={() => this.setState({
                            showModal: false,
                            fullName: '',
                            email: '',
                            password: '',
                            role: '',
                            _id: ''
                        })}>Close Modal</button>
                    </ReactModal>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.confirmUsersList()}
                        </tbody>
                    </table>
                    <ReactPaginate
                        previousLabel={'prev'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        onPageChange={this.handlePageClick.bind(this)}
                        pageCount={Math.ceil(this.props.paginate.lengthUsersArray / this.props.paginate.itemsPerPage)}
                        marginPagesDisplayed={this.props.currentPage}
                        pageRangeDisplayed={2}
                        containerClassName={'pagination'}
                        activeClassName={'active'}
                    ></ReactPaginate>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: { confirmUsers: IUserModel; paginate: Paginate; }) {
    return {
        confirmUsers: state.confirmUsers,
        paginate: state.paginate
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        confirmUsersAction: confirmUsersAction,
        paginateAction: paginateAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminUsers);