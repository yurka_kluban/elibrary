import React, { Component } from 'react';
import BookService from '../../../../shared/services/bookService';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { IBookModel } from '../../../../../../../shared/models';

export const getBookByIdAction = (book: IBookModel) => {
    return {
        type: "BOOK_BY_ID",
        payload: book
    }
};

export const getBookById = (state: IBookModel, action: { type: string; payload: IBookModel; }) => {
    switch (action.type) {
        case "BOOK_BY_ID":
            return action.payload
        
        default:
            return state;
    }
}

class ShowBook extends Component<any, any> {

    public bookService:BookService = new BookService();

    public id: string = '';
    public book: IBookModel[] = [];

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {
        this.getBook();
    }

    public getBook(): void {
        // this.id = this.props.location.query.bookId;
        this.id = location.pathname.split('/')[4];
        this.bookService.getById(this.id).then((res: {data: Object}) => {
            this.props.getBookByIdAction(res.data);
        })
    }

    public bookDetails() {
        return (
            <React.Fragment>
                <img className="mr-6" alt="Generic placeholder image" src={this.props.book.image} />
                <ul className="list-group">
                    <li className="list-group-item list-group-item-light">Title: {this.props.book.title}</li>
                    <li className="list-group-item list-group-item-light">Author:{this.props.book.author.map((item: any, index: any) => {
                        return <a key={index}>{item.name}</a>
                    })}</li>
                    <li className="list-group-item list-group-item-light">Language: {this.props.book.language}</li>
                    <li className="list-group-item list-group-item-light">Published Year: {this.props.book.published}</li>
                    <li className="list-group-item list-group-item-light">Number of pages: {this.props.book.pages}</li>
                </ul>
            </React.Fragment>
        )
    }

    render() {
        console.log(this.props)
        return (
            <div className="media">
                <div className="media-body">
                    {this.bookDetails()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state: { book: IBookModel; }) {
    return {
        book: state.book
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({ getBookByIdAction: getBookByIdAction }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowBook);