import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import './admin-books.scss';
import BookService from '../../../shared/services/bookService';
import { Link } from 'react-router-dom';
import ReactModal from 'react-modal';
import JournalsService from '../../../shared/services/journalsService';
import AuthorService from '../../../shared/services/authorService';
import { IBookModel, IAuthorModel, IJournalModel } from '../../../../../../shared/models';
import { Paginate } from '../../../shared/models';

export const paginateAction = (usersPaginate: Paginate) => {
    return {
        type: "PAGINATE",
        payload: usersPaginate
    }
}

export const paginate = (state:Paginate, action: { type: string; payload: Paginate; }) => {
    switch (action.type) {
        case "PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

export const authorsAction = (authors: IAuthorModel[]) => {
    return {
        type: "GET_AUTHORS",
        payload: authors
    }
}

export const authors = (state:IAuthorModel[], action: { type: string; payload: IAuthorModel; }) => {
    switch (action.type) {
        case "GET_AUTHORS":
            return action.payload
        
        default:
            return state;
    }
}

export const journalsAction = (journals: IJournalModel[]) => {
    return {
        type: "GET_JOURNALS",
        payload: journals
    }
}

export const journals = (state:IJournalModel[], action: { type: string; payload: IJournalModel; }) => {
    switch (action.type) {
        case "GET_JOURNALS":
            return action.payload
        
        default:
            return state;
    }
}

export const booksPaginateAction = (books: IBookModel) => {
    return {
        type: "BOOKS_PAGINATE",
        payload: books
    }
}

export const booksPaginate = (state: IBookModel[], action: { type: string; payload: IBookModel; }) => {
    switch (action.type) {
        case "BOOKS_PAGINATE":
            return action.payload
        
        default:
            return state;
    }
}

class AdminBooks extends Component<any, any> {

    public journalService:JournalsService = new JournalsService();
    public authorService:AuthorService = new AuthorService();
    public bookService:BookService = new BookService();

    public authors: string[] = [];    
    public journals: string[] = [];  
    public lengthBooksArray: number | undefined;
    public currentPage:number = 1;
    public itemsPerPage:number = 2;
    public imgBase: string = '';
    public timer: any;

    constructor(props: any) {
        super(props);
        this.state = {
            title: '',
            language: '',
            published: 1,
            pages: 1,
            searchValue: '',
            showModal: false,
            _id: '',
            image: ''
        };
        this.handleFile = this.handleFile.bind(this);
        this.chooseJournal = this.chooseJournal.bind(this);
    }

    componentWillMount() {
        ReactModal.setAppElement('body');
        this.getBooks();
        this.getAuthors();
        this.getJournals();
    }

    public onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({ [event.target.name]: event.target.value });
    };

    public modalState(): boolean {
        const stateRef = this.state;
        
        return stateRef["showModal"];
    }

    public getBooks(): void {
        this.bookService.getAllForPagination(this.itemsPerPage, this.currentPage, this.state.searchValue).then(res => {
            this.props.booksPaginateAction(res.data.books as IBookModel[]);
            this.lengthBooksArray = res.data.length[0].length;
            const paging = {
                itemsPerPage: this.itemsPerPage,
                currentPage: this.currentPage,
                lengthBooksArray: this.lengthBooksArray,
                searchValue: this.state.searchValue
            }
            this.props.paginateAction(paging);
        });
    }

    public getAuthors(): void {
        this.authorService.getAll().then(res => {
            this.props.authorsAction(res.data as IAuthorModel);
        });
    }

    public getJournals(): void {
        this.journalService.getAll().then(res => {
            this.props.journalsAction(res.data as IJournalModel);
        });
    }

    public delete(event: any): void {
        this.bookService.delete(event.target.id).then(res => {
            console.log(res);
            this.getAllBooksForPaging();
        })
    }

    public handleFile(event: any): void {
        let file = event.target.files[0];
        let uploadImg = (image: string) => {
            this.imgBase = image;
        }
        let myReader = new FileReader();
        myReader.onloadend = (e) => {
            let img = e.target as any;
            uploadImg(img.result);
        };
        myReader.readAsDataURL(file);
    }

    public books() {
        return (
            <React.Fragment>{this.props.books.map((item: any, index: number) => {
                return (
                    <div className="col-3" key={index + 1}>
                        <div className="card" >
                            <img src={item.image} alt="Card image cap" className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">Title: <Link key={index} to={{ pathname: '/admin/admin-books/show-book/' + item._id, query: { bookId: item._id } }}> {item.title}</Link></h5>
                                <p className="card-text" key={index + 1}>Author: {item.author.map((item: IAuthorModel, index: number) => {
                                    return (
                                        <Link key={index} to={'/admin/admin-books/show-author/' + item._id}> {item.name}</Link>
                                    )
                                })}</p>
                            </div>
                            <button className="btn btn-warning" id={item._id} onClick={() => this.setState({
                                showModal: true, 
                                _id: item._id, 
                                title: item.title, 
                                language: item.language,
                                published: item.published,
                                pages: item.pages 
                            })}>Edit</button>
                            <button type="button" className="btn btn-danger" id={item._id} onClick={() => {
                                this.delete(event)
                            }}>Delete</button>
                        </div>
                    </div>
                )
            })
            }
            </React.Fragment>
        )
    }

    public getAllBooksForPaging(): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.getBooks();
        }, 500);
    }

    public handlePageClick(data: { selected: number; }): void {
        this.currentPage = data.selected + 1;
        this.getAllBooksForPaging();
    }

    public updateInputValue(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            searchValue: event.target.value
        });
        this.getAllBooksForPaging();
    }

    public submit = (event: { preventDefault: () => void; }) => {
        event.preventDefault();
        const book = {
            title: this.state.title,
            language: this.state.language,
            image: this.imgBase,
            published: this.state.published,
            pages: this.state.pages,
            author: this.authors,
            journal: this.journals
        }
        if (this.state._id === "") {
            this.bookService.add(book).then(() => {
                this.getAllBooksForPaging();
            })
        }
        this.bookService.update(this.state._id, book).then(res => {
            console.log(res);
            this.getAllBooksForPaging();
        })
    }

    public chooseAuthor(event: React.ChangeEvent<HTMLSelectElement>): void {
        this.authors.push(event.target.value)
    }

    public chooseJournal(event: React.ChangeEvent<HTMLSelectElement>): void {
        this.journals.push(event.target.value)
    }

    render() {
        return (
            <div>
                <div className="active-cyan-3 active-cyan-4 mb-4">
                    <input className="form-control" type="text" 
                    placeholder="Search" value={this.state.searchValue} 
                    onChange={event => this.updateInputValue(event)} />
                </div>
                <button type="submit" className="btn btn-primary" onClick={() => this.setState({
                    showModal: true
                })}>Add Book</button>
                <ReactModal
                    isOpen={this.modalState()}
                    onRequestClose={() => this.setState({ showModal: false })}
                    contentLabel="Minimal Modal Example">
                    <form onSubmit={this.submit}>
                        <div className="form-group">
                            <label htmlFor="name">Title</label>
                            <input
                            onChange={this.onChange}
                            defaultValue={this.state.title} 
                            type="text" className="form-control" 
                            name="title" />
                        </div>
                        <div className="form-group">
                            <label className="image-upload-container btn btn-bwm">
                                <input 
                                onChange={this.handleFile}
                                defaultValue={this.imgBase} 
                                name="image" 
                                type="file" />
                            </label>
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Language</label>
                            <input 
                            onChange={this.onChange}
                            defaultValue={this.state.language} 
                            type="text" 
                            className="form-control" 
                            name="language" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Published Year</label>
                            <input 
                            onChange={this.onChange}
                            defaultValue={this.state.published}
                            type="number" 
                            className="form-control" 
                            name="published" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Number of pages</label>
                            <input 
                            onChange={this.onChange}
                            defaultValue={this.state.pages}
                            type="number" 
                            className="form-control" 
                            name="pages" />
                        </div>
                        <div className="input-group mb-3">
                            <select name="author" onChange={(event) => this.chooseAuthor(event)}>
                                {this.props.authors.map((item: any, index: any) =>
                                    <option
                                        id={item._id} key={index + 1} value={item._id}>{item.name}
                                    </option>
                                )}
                            </select>
                        </div>
                        <div className="input-group mb-3">
                            <select name="journal" onChange={(event) => this.chooseJournal(event)}>
                                {this.props.journals.map((item: any, index: any) =>
                                    <option
                                        id={item._id} key={index + 1} value={item._id}>{item.title}
                                    </option>
                                )}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-success">Update</button>
                    </form>
                    <button onClick={() => this.setState({ 
                        showModal: false,
                        _id: '', 
                        title: '', 
                        language: '',
                        published: '',
                        pages: ''
                    })}>Close Modal</button>
                </ReactModal>
                <div className="container-fluid">
                    <div className="row">
                        {this.books()}
                    </div>
                </div>
                <ReactPaginate
                    previousLabel={'prev'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    onPageChange={this.handlePageClick}
                    pageCount={Math.ceil(this.props.paginate.lengthBooksArray / this.props.paginate.itemsPerPage)}
                    marginPagesDisplayed={this.props.currentPage}
                    pageRangeDisplayed={2}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                ></ReactPaginate></div>
        )
    }
}

function mapStateToProps(state: { authors: IAuthorModel; journals: IJournalModel; books: IBookModel; paginate: Paginate; }) {
    console.log(state)
    return {
        authors: state.authors,
        journals: state.journals,
        books: state.books,
        paginate: state.paginate
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        booksPaginateAction: booksPaginateAction,
        paginateAction: paginateAction,
        authorsAction: authorsAction,
        journalsAction: journalsAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminBooks);