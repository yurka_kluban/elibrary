import React, { Component } from 'react';
import './admin-home.scss';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch, Reducer } from 'redux';
import AdminService from '../../../shared/services/adminService';
import { IUserModel } from '../../../../../../shared/models';

export const unconfirmUsersAction = (users: IUserModel[]) => {
    return {
        type: "UNCONFIRM_USERS",
        payload: users
    }
}

export function unconfirm(state: IUserModel[], action: { type: string, payload: IUserModel }) {
    switch (action.type) {
        case "UNCONFIRM_USERS":
            return action.payload
        
        default:
            return state;
    }
}

class AdminHome extends Component<any , any> {

    public adminService: AdminService = new AdminService();

    public statusMessage: string = '';

    constructor(props: any) {
        super(props);
    }

    componentWillMount() {
        this.get();
    }
    
    public get(): void {
        this.adminService.getUnconfirmUsers().then(res => {
            this.props.unconfirmUsersAction(res.data as IUserModel[]);
        })
    }
    
    public confirm(event: any): void {
        this.adminService.confirm(event.target.id).then(() => {
            this.get();
        })
    }
    
    public reject(event: any): void {
        this.adminService.delete(event.target.id).then(() => {
            this.statusMessage = 'Data successfully deleted';
            this.get();
        })
    }

    public confirmUsersList() {
        return [].map.call(this.props.users, (item: IUserModel, index: number) => {
            return (
                <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>
                        {item.fullName}
                    </td>
                    <td>
                        {item.email}
                    </td>
                    <td>
                        {item.role}
                    </td>
                    <td>
                        <button type="button" className="btn btn-info" id={item._id} onClick = {()=>{
                        this.confirm(event)
                    }}>Confirm</button>
                    </td>
                    <td>
                        <button type="button" className="btn btn-danger" id={item._id} onClick = {()=>{
                        this.reject(event)
                    }}>Reject</button>
                    </td>
                </tr>
            )
        });
    }

    render() {
        return (
            <div className="container">
                <h2>New Users</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.confirmUsersList()}
                    </tbody>
                </table>
            </div>
        )
    }
}

function mapStateToProps(state: { unconfirmUsers: IUserModel[]; }) {
    return {
        users: state.unconfirmUsers
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({ unconfirmUsersAction: unconfirmUsersAction }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminHome);