import React, { Component } from 'react';
import AuthService from '../../shared/services/authService'
import './login.scss'
import { Link } from 'react-router-dom';
import { IUserModel } from '../../../../../shared/models';

export default class Login extends Component<any, any> {

  public authService:AuthService = new AuthService();

  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirm: true
    };
  }

  public onChange = (event: { target: { name: string; value: string; }; }) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  public onSubmit = (event: { preventDefault: () => void; }) => {
    event.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password,
      confirm: this.state.confirm
    };
    this.authService.login(userData).then(res => {
      localStorage.setItem('token', res.data.token);
      const userInfo: IUserModel = res.data.user;
      userInfo.role === 1 ? this.props.history.push('/user') : this.props.history.push('/admin');
    })
    if (!(userData.email && userData.password)) {
      return;
    }
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <h2>Log In!</h2>
          <div className="img">
          </div>
          <div className='form-group'>
            <label htmlFor="email">Email</label>
            <input type="email" className="form-control" 
              name="email" 
              value={this.state.email}
              onChange={this.onChange} 
              id="exampleInputEmail1" aria-describedby="emailHelp"
              placeholder="Enter your E-mail" />
          </div>
          <div className='form-group'>
            <label htmlFor="password">Password</label>
            <input type="password" className="form-control" 
              name="password" 
              onChange={this.onChange}
              value={this.state.password}
              placeholder="Password" />
          </div>
          <button type="submit" className="btn btn-primary">Login</button>
          <Link to='/register'>Registration</Link>
        </form>
      </div>
    );
  }
}

