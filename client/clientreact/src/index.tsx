import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import * as serviceWorker from './serviceWorker';
import createHistory from "history/createBrowserHistory";
import saga from './sagas/auth'
import App from './App';
import './index.scss';
import 'bootstrap/scss/bootstrap.scss'
import { Router, Switch } from 'react-router-dom';
import allReducers from './reducers';

const sagaMiddleware = createSagaMiddleware()

const history = createHistory();

const middleware = applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware
);

const store = createStore(allReducers, middleware);

sagaMiddleware.run(saga);

ReactDOM.render(
    <Provider store={store}>
        <Router history={createHistory()}>
            <Switch>
                <App />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
