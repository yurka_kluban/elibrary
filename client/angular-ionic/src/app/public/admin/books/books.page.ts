import { Component, OnInit } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { LoadingController } from '@ionic/angular';
import { BookService } from 'src/app/services/book.service';
import { AuthorService } from 'src/app/services/author.service';
import { JournalService } from 'src/app/services/journal.service';
import { IBookModel, IAuthorModel, IJournalModel } from '../../../../../../../shared/models';

@IonicPage()
@Component({
  selector: 'app-books',
  templateUrl: './books.page.html',
  styleUrls: ['./books.page.scss'],
})
export class BooksPage implements OnInit {

  public books: Array<IBookModel> = [];
  public author: Array<IAuthorModel> = [];
  public journals: Array<IJournalModel> = [];

  constructor(private bookService: BookService,
    private authorService: AuthorService ,
    private journalService: JournalService,
    private loadingController: LoadingController) { }

  ngOnInit() {
    this.getBooks();
  }

  async getBooks() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      spinner: 'crescent',
      duration: 1000
    });
    await loading.present();
    await this.bookService.getAllForPagination(1, 2, '').subscribe((res: IBookModel | any) => {
        console.log(res);
        this.books = res.books;
        loading.dismiss();
      }, (err: string) => {
        console.log(err);
        loading.dismiss();
      });
  }

}
