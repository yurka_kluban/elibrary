import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  bookForm: FormGroup;
  authors: FormArray;

  constructor(public bookService: BookService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
  }

  // async getClassroom(id: any) {
  //   const loading = await this.loadingController.create({
  //     message: 'Please wait...',
  //     spinner: 'crescent',
  //     duration: 1000
  //   });
  //   await loading.present();
  //   await this.bookService.getById(id).subscribe(res => {
  //     this.bookForm.controls['title'].setValue(res);
  //     let controlArray = <FormArray>this.bookForm.controls['authors'];
  //     res.author.forEach(std => {
  //       controlArray.push(this.formBuilder.group({
  //          student_name: ''
  //       }));
  //     });
  //     for(let i=0;i<res.students.length;i++) {
  //       controlArray.controls[i].get('student_name').setValue(res.students[i].student_name);
  //     }
  //     console.log(this.classroomForm);
  //     loading.dismiss();
  //   }, err => {
  //     console.log(err);
  //     loading.dismiss();
  //   });
  // }

}
