import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { IBookModel } from '../../../../../../../../shared/models';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  public book: Array<IBookModel> = [];
  location: any;

  constructor(public bookService: BookService,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.getBooks();
  }

  async getBooks() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
      spinner: 'crescent',
      duration: 1000
    });
    await loading.present();
    await this.bookService.getById(this.route.snapshot.paramMap.get('id')).subscribe((res: IBookModel[]) => {
        console.log(res);
        this.book = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async delete(id: any) {
    const loading = await this.loadingController.create({
      message: 'Deleting...',
      spinner: 'crescent',
      duration: 1000
    });
    await loading.present();
    await this.bookService.delete(id)
      .subscribe(res => {
        loading.dismiss();
        this.location.back();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

}
