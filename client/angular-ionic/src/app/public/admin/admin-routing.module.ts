import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminPage } from './admin.pages';


const routes: Routes = [
    {
        path: 'admin',
        component: AdminPage,
        children: [
            {
                path: 'home',
                loadChildren: './home/home.module#HomePageModule'
            },
            {
                path: 'books',
                loadChildren: './books/books.module#BooksPageModule'
            },
            {
                path: 'authors',
                loadChildren: './authors/authors.module#AuthorsPageModule'
            },
            {
                path: 'journals',
                loadChildren: './journals/journals.module#JournalsPageModule'
            }
        ]
    },
    {
        path: '',
        redirectTo: 'admin/home',
        pathMatch: 'full'
    },
  { path: 'details/:id', loadChildren: './books/details/details.module#DetailsPageModule' },
  { path: 'edit/:id', loadChildren: './books/edit/edit.module#EditPageModule' }


    //   {
    //     path: 'detail/:id',
    //     loadChildren: './detail/detail.module#DetailPageModule'
    //   },
    //   {
    //     path: 'edit/:id',
    //     loadChildren: './edit/edit.module#EditPageModule'
    //   },
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
