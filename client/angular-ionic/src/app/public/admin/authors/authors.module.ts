import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AuthorsPage } from './authors.page';


@NgModule({
  imports: [
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: AuthorsPage
      }
    ])
  ],
  declarations: [AuthorsPage]
})
export class AuthorsPageModule {}
