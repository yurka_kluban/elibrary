import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'admin-root',
  templateUrl: './admin.pages.html',
  styleUrls: ['./admin.pages.scss'],
})
export class AdminPage implements OnInit {

  public pages = [
    {
      title: 'Home',
      url: 'home',
      icon: 'home'
    },
    {
      title: 'Books',
      url: 'books',
      icon: 'book'
    },
    {
      title: 'Authors',
      url: 'authors',
      icon: 'person'
    },
    {
      title: 'Journals',
      url: 'journals',
      icon: 'images'
    }
  ]

  constructor(private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar) {
      this.initializeApp();
     }

  ngOnInit() {
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

}
