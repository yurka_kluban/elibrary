import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { JournalsPage } from './journals.page';



@NgModule({
  imports: [
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: JournalsPage
      }
    ])
  ],
  declarations: [JournalsPage]
})
export class JournalsPageModule {}
