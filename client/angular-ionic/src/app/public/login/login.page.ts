import { Component, OnInit, Injectable } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Loading, IonicPage } from 'ionic-angular';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Observable } from 'rxjs';



@IonicPage()
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
@Injectable()
export class LoginPage implements OnInit {
  
  authenticationState: Observable<boolean>;
  loading: Loading;
  registerCredentials = { email: '', password: '' };

  constructor(private authService: AuthenticationService,
    private router: Router,
    private localStorageService: LocalStorageService,) {

  }

  ngOnInit() {
    this.authenticationState = this.authService.getAuthStateObserver();
  }


  public login() {
      console.log(this.registerCredentials)
      this.authService.login(this.registerCredentials).subscribe(allowed => {
        // console.log(allowed)
        // if (allowed) {
          this.localStorageService.setToken(allowed['token']);  
          // const userInfo: any = allowed;      
          // userInfo.user.role === 1 ? this.router.navigateByUrl('/profile') : this.router.navigateByUrl('/admin');
          this.router.navigateByUrl('/admin')
      //   } else {
      //     this.showError("Access Denied");
      //   }
      // },
      //   error => {
      //     this.showError(error);
      //   });
  })
}
  // public showError(text: string) {
  //   this.loading.dismiss();
  // }


}
