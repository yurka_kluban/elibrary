import { Component, OnInit } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@IonicPage()
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  createSuccess = false;
  registerCredentials = { fullName: '', email: '', password: '', confirm: false };

  constructor( 
    private authService: AuthenticationService,private router: Router) { }

  ngOnInit() {
  }

  public register() {
    this.authService.register(this.registerCredentials).subscribe(success => {
      console.log(success)
      this.router.navigateByUrl('/login');
  })
}


}
