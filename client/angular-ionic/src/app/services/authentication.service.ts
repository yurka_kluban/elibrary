import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IUserModel } from '../../../../../shared/models';
import { environment } from '../../environments/environment';

const token = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticationState = new BehaviorSubject(false);

  constructor(private storage: Storage, private plt: Platform, private http: HttpClient) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  public checkToken() {
    this.storage.get(token).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    })
  }

  public async logout() {
    await this.storage.remove(token);
    this.authenticationState.next(false);
  }

  public isAuthenticated() {
    return this.authenticationState.value;
  }

  public getAuthStateObserver(): Observable<boolean> {

    return this.authenticationState.asObservable();
  }

  public register(user: IUserModel | any) {
    const body = {
      fullName: user.fullName,
      email: user.email,
      password: user.password,
      confirm: user.confirm
    };
    return this.http.post(environment.url + '/auth/register', body);
  }

  public login(user: IUserModel | any) {
    this.storage.set(token, 'token').then( () => {
      this.authenticationState.next(true);
    })
    const body = {
      email: user.email,
      password: user.password,
      confirm: user.confirm
    };
    return this.http.post(environment.url + '/auth/login', body);
  }

}