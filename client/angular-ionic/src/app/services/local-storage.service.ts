import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private storage: Storage) { }

  // public setToken(token: string): void {
  //   localStorage.setItem('token', token);
  // }

  // public getToken() {
  //   return localStorage.getItem('token');
  // }

  // public deleteToken(): void {
  //   localStorage.removeItem('token');
  // }

  public setToken(token: string) {
    return this.storage.set('token', token);
  }

  async getToken() {
    return await this.storage.get('token');
  }

  public deleteToken() {
    this.storage.remove('token');
  }
  
}
