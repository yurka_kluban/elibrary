import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string) {
    const body = {
      itemsPerPage: itemsPerPage,
      currentPage: currentPage,
      searchValue: searchValue
    };
    return this.http.post(environment.url + '/book/paging', body);
  }

  public getById(bookId: string) {
    return this.http.get(environment.url + "/book/" + bookId);
  }

  public delete(bookId: string) {
    return this.http.delete(environment.url + "/book/" + bookId);
  }
}
