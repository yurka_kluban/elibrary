
export const environment = {
  url: "https://localhost:5000",
  auth: {
    clientID: "XW3yte9CfmareK5Qe2RgrxYWVQNRHMLL",
    domain: "e-library-social.auth0.com", 
    redirect: "http://localhost:4200/auth/login",
    audience: "http://localhost:4200",
    scope: 'openid profile email'
  },
  production: false,
};

