import { Component } from '@angular/core';
import { UserService } from './shared/services/user.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private userService: UserService,
    private router: Router) { 
    this.checkUser();
  }

  public checkUser() {
    this.userService.getUser().subscribe((data: any) => {
      if (data.role == 1) {
        this.router.navigate(['/profile']);
      }
      if (data.role == 2) {
        this.router.navigate(['/admin']);
      }
      err => {
      }
    })

  }

}


