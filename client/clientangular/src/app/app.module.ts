import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { AppComponent } from './app.component';
import { HomeComponent } from './component/auth/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './shared/guadrs/auth.guard';
import { AuthorService } from './shared/services/author.service';
import { BookService } from './shared/services/book.service';
import { UserService } from './shared/services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthNavbarComponent } from './container/auth-navbar/auth-navbar.component';
import { AdminService } from './shared/services/admin.service';
import { AuthServices } from './shared/services/auth.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { HeadersInterceptor } from './shared/interceptor/headers.interceptor';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from "angular-6-social-login";

 
 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("Your-Facebook-app-id")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("429085464236-28e9v1hfgmgp4tvg1kv96lftsqlrbh58.apps.googleusercontent.com")
        },
          {
            id: LinkedinLoginProvider.PROVIDER_ID,
            provider: new LinkedinLoginProvider("1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com")
          },
      ]
  )
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthNavbarComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    SocialLoginModule
  ],
  providers: [
    AuthServices,
    AdminService,
    AuthorService,
    BookService,
    UserService,
    AuthGuard,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeadersInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }