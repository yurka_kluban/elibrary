import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from '../services/localStorage.service';



@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private _localStorage: LocalStorageService
        ) { }

    canActivate 
        (next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) : boolean {
            if (!this._localStorage.isLoggedIn()) {
                this.router.navigateByUrl('/login');
                this._localStorage.deleteToken();
                return false;
            }
            return true;    
    }

}
