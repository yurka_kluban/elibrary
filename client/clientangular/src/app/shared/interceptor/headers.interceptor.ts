import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from '../services/localStorage.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

    constructor( public _localStorage: LocalStorageService ) {}


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!(req.url.indexOf('/auth/login') === -1)) {
            return next.handle(req);
        }
        if (!(req.url.indexOf('/auth/register') === -1)) {
            return next.handle(req);
        }
        const paramReq = req.clone({
            headers: new HttpHeaders ({'x-access-token' : this._localStorage.getToken()})
        });
        return next.handle(paramReq);
    }
}