import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import * as auth0 from 'auth0-js';
import { Router } from '@angular/router';



@Injectable({
    providedIn: 'root'
})

export class AuthServices {

    auth0 = new auth0.WebAuth({
        clientID: environment.auth.clientID,
        domain: environment.auth.domain,
        responseType: 'token',
        redirectUri: environment.auth.redirect,
        audience: environment.auth.audience,
        scope: environment.auth.scope
      });
      // Store authentication data
      userProfile: any;
      accessToken: string;
      authenticated: boolean;
    

    constructor(private http: HttpClient,private router: Router) { 
        this.getAccessToken();
    }

    socialLogin() {
        this.auth0.authorize();
    }

    handleLoginCallback() {
        // When Auth0 hash parsed, get profile
        this.auth0.parseHash((err, authResult) => {
          if (authResult && authResult.accessToken) {
            window.location.hash = '';
            this.getUserInfo(authResult);
          } else if (err) {
            console.error(`Error: ${err.error}`);
          }
          this.router.navigate(['/']);
        });
      }
    
      getAccessToken() {
        this.auth0.checkSession({}, (err, authResult) => {
          if (authResult && authResult.accessToken) {
            this.getUserInfo(authResult);
          } else if (err) {
            console.log(err);
            this.logout();
            this.authenticated = false;
          }
        });
      }
    
      getUserInfo(authResult) {
        // Use access token to retrieve user's profile and set session
        this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
          if (profile) {
            this._setSession(authResult, profile);
          }
        });
      }
    
      private _setSession(authResult, profile) {
        const expTime = authResult.expiresIn * 1000 + Date.now();
        // Save authentication data and update login status subject
        localStorage.setItem('expires_at', JSON.stringify(expTime));
        this.accessToken = authResult.accessToken;
        this.userProfile = profile;
        this.authenticated = true;
      }
    
      logout() {
        // Remove auth data and update login status
        localStorage.removeItem('expires_at');
        this.userProfile = undefined;
        this.accessToken = undefined;
        this.authenticated = false;
      }
    
      get isLoggedIn(): boolean {
        // Check if current date is before token
        // expiration and user is signed in locally
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return Date.now() < expiresAt && this.authenticated;
      }
    

    public register(user: IUserModel): Observable<Object> {
        const body = {
            fullName: user.fullName,
            email: user.email,
            password: user.password,
            confirm: user.confirm
        };
        return this.http.post(environment.url + '/auth/register', body);
    }

    public login(user: IUserModel): Observable<Object> {
        const body = {
            email: user.email,
            password: user.password,
            confirm: user.confirm
        };
        return this.http.post(environment.url + '/auth/login', body);
    }

}