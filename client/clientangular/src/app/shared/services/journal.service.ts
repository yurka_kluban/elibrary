import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IJournalModel } from '../../../../../../shared/models/journal.model';



@Injectable({
    providedIn: 'root'
})

export class JournalService {

    constructor(private http: HttpClient) { }

    public getAll(): Observable<Object> {
        return this.http.get(environment.url + "/journal");
    }

    public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string): Observable<Object> {
        const body = {
          itemsPerPage: itemsPerPage,
          currentPage: currentPage,
          searchValue: searchValue
        };
        return this.http.post(environment.url + '/journals/paging', body);
      }

    public add(journal: IJournalModel): Observable<Object> {
        const body = { id: journal._id, title: journal.title };
        return this.http.post(environment.url + '/journal', body);
    }

    public update(_id: string, title: string): Observable<Object> {
        const body = {
            title: title
        }
        return this.http.put(environment.url + "/journal/" +_id, body);
    }

    public delete(journalId: string): Observable<Object> {
        return this.http.delete(environment.url + "/journal/" + journalId);
    }

}