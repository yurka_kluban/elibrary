import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})


export class LocalStorageService {

    public setToken(token: string): void {
        localStorage.setItem('token', token);
    }

    public getToken() {
        return localStorage.getItem('token');
    }

    public deleteToken(): void {
        localStorage.removeItem('token');
    }

    public getUserPayload() {
        var token = this.getToken();
        if (token) {
            var userPayload = atob(token.split('.')[1]);
            return JSON.parse(userPayload);
        }
        return;
    }

    public isLoggedIn() {
        var userPayload = this.getUserPayload();
        if (userPayload)
            return userPayload.exp > Date.now() / 1000;
        else
            return false;
    }
}