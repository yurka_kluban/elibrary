import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public get(): Observable<Object> {
    return this.http.get(environment.url + "/profile");
  }

  public getById(_id: string): Observable<Object> {
    return this.http.get(environment.url + "/profile/" + _id);
  }

  public getUser(): Observable<Object> {
    return this.http.get(environment.url + "/auth/profile")
  }

  public update(id: string, user: IUserModel): Observable<Object> {
    const body = {
      fullName: user.fullName,
      email: user.email,
      password: user.password,
      confirm: user.confirm
    }
    return this.http.put(environment.url + "/profile/" +id, body);
  }

}
