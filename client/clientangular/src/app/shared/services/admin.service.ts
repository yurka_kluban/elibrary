import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})

export class AdminService {

    constructor(private http: HttpClient) { }

    public get(): Observable<Object> {
        return this.http.get(environment.url + "/admin");
    }
    
    public getUnconfirmUsers(): Observable<Object> {
        return this.http.get(environment.url + "/admin/unconfirmUsers");
    }

    public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string): Observable<Object> {
        const body = {
          itemsPerPage: itemsPerPage,
          currentPage: currentPage,
          searchValue: searchValue
        };
        return this.http.post(environment.url + '/admin/users/paging', body);
      }
      
      public add(user: IUserModel): Observable<Object> {
          const body = {
              fullName: user.fullName,
              email: user.email,
              password: user.password,
              confirm: user.confirm,
              role: user.role
            };
        return this.http.post(environment.url + '/admin', body);
    }
    
    public confirm(id: string): Observable<Object> {
        const body = { confirm: true }
        return this.http.put(environment.url + "/admin/confirmOrBan/" + id, body);
    }
    
    public ban(id: string): Observable<Object> {
        const body = { confirm: false }
        return this.http.put(environment.url + "/admin/confirmOrBan/" + id, body);
    }

    public update(id: string, user: IUserModel): Observable<Object> {
        const body = {
            fullName: user.fullName,
            email: user.email,
            password: user.password,
            confirm: user.confirm,
            role: user.role
        }
        return this.http.put(environment.url + "/admin/" + id, body);
    }
    
    public delete(id: string): Observable<Object> {
        return this.http.delete(environment.url + "/admin/" + id);
    }
}  