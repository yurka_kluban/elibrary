import { Injectable } from '@angular/core';
import { IAuthorModel } from "../../../../../../shared/models/author.model";
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) { }

  public getAuthor(id: string): Observable<Object> {
    return this.http.get(environment.url + "/author/" + id);
  }

  public getAllAuthor(): Observable<Object> {
    return this.http.get(environment.url + "/author");
  }

  public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string): Observable<Object> {
    const body = {
      itemsPerPage: itemsPerPage,
      currentPage: currentPage,
      searchValue: searchValue
    };
    return this.http.post(environment.url + '/authors/paging', body);
  }

  public add(author: IAuthorModel): Observable<Object> {
    const body = { id: author._id, name: author.name };
    return this.http.post(environment.url + '/author', body);
  }

  public update(id: string, name: string): Observable<Object> {
    const body = {
      name: name
    };
    return this.http.put(environment.url + "/author/" + id, body);
  }

  public delete(authorId: string): Observable<Object> {
    return this.http.delete(environment.url + "/author/" + authorId);
  }

}
