import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IBookModel } from '../../../../../../shared/models/book.model'
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  public getBooks(): Observable<Object> {
    return this.http.get(environment.url + "/book");
  }

  public getById(bookId: string): Observable<Object> {
    return this.http.get(environment.url + "/book/" + bookId);
  }

  // public getBookForEdit(bookId: string): Observable<Object> {
  //   return this.http.get(environment.url + "/book/edit/" + bookId);
  // }

  public getAllForPagination(itemsPerPage: number, currentPage: number, searchValue: string): Observable<Object> {
    const body = {
      itemsPerPage: itemsPerPage,
      currentPage: currentPage,
      searchValue: searchValue
    };
    return this.http.post(environment.url + '/book/paging', body);
  }

  public getByAuthorId(id: string): Observable<Object> {
    return this.http.get(environment.url + '/book/author/'+id)
  }

  public add(book: IBookModel): Observable<Object> {
    const body = {
      title: book.title,
      image: book.image,
      language: book.language,
      published: book.published,
      pages: book.pages,
      author: book.author,
      journal: book.journal
    }
    return this.http.post(environment.url + '/book', body);
  }

  public update(bookId: string, book: IBookModel): Observable<Object> {
    const body = {
      title: book.title,
      image: book.image,
      language: book.language,
      published: book.published,
      pages: book.pages,
      author: book.author,
      journal: book.journal
    }
    return this.http.put(environment.url + "/book/" + bookId, body);
  }

  public delete(bookId: string): Observable<Object> {
    return this.http.delete(environment.url + "/book/" + bookId);
  }

}
