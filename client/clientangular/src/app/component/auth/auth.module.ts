import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthNavbarComponent } from 'src/app/container/auth-navbar/auth-navbar.component';
import { MatTableModule } from '@angular/material/table';


@NgModule({
    declarations: [
        AuthComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AuthNavbarComponent,

    ],
    imports: [
        AuthRoutingModule,
        MatTableModule
    ],
    providers: [
    ],

})
export class AuthModule { }