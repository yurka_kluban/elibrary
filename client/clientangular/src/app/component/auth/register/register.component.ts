import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServices } from 'src/app/shared/services/auth.service';
import { LocalStorageService } from 'src/app/shared/services/localStorage.service';



@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public statusMessage: string = '';

  public signUpForm: FormGroup = new FormGroup({
    fullName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    confirm: new FormControl(false) 
  })

  constructor(
    private _localStorage: LocalStorageService,
    private _authService: AuthServices,
    private router: Router
  ) { }

  ngOnInit() {

  }


  public submit(event: any): void {
    this._authService.register(this.signUpForm.value).subscribe(data => {
      this._localStorage.setToken(data['token']);
        this.router.navigateByUrl('/auth/login');
      },
      err => {
        (err.status == 400) ? this.statusMessage = "User already exists with this email" : this.statusMessage = "Error on the server";
      })   ;   
  }
}

