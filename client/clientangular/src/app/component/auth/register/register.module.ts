import { NgModule } from '@angular/core';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';



@NgModule ({
    imports: [
        RegisterRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatTableModule
    ],
    declarations: [RegisterComponent]
})

export class RegisterModule { }