import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServices } from 'src/app/shared/services/auth.service';
import { LocalStorageService } from 'src/app/shared/services/localStorage.service';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider
} from 'angular-6-social-login';
import * as jwt_decode from "jwt-decode";




@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public authLogin: String = '';
  public statusMessage: string = '';

  orderForm: FormGroup;
  items: FormArray;


  public logInForm: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required, Validators.minLength(6)])
  });

  constructor(
    private _authService: AuthServices,
    private _localStorage: LocalStorageService,
    private router: Router,
    private socialAuthService: AuthService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      customerName: '',
      email: '',
      items: this.formBuilder.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      name: ['',Validators.required],
      description: ['',Validators.required],
      price: ['',Validators.required]
    });
  }

  addItem(): void {
    this.items = this.orderForm.get('items') as FormArray;
    this.items.push(this.createItem());
    console.log(this.items)
  }

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        let token = jwt_decode(userData.idToken)
        console.log(token);
        Object.assign(userData, {role: 1});
        console.log(socialPlatform+" sign in data : " , userData);
        // this._authService.login(userData).subscribe(data => {
        //   console.log(data)
        //   userData.role === 1 ? this.router.navigateByUrl('/profile') : this.router.navigateByUrl('/admin');
        // })    
      }
    );
  }

  public submit(): void {
    this._authService.login(this.logInForm.value).subscribe(data => {
      this._localStorage.setToken(data['token']);
      const userInfo: any = data;
      userInfo.user.role === 1 ? this.router.navigateByUrl('/profile') : this.router.navigateByUrl('/admin');
      if (userInfo.user.confirm === false) {
        this.router.navigateByUrl('/home');
        this.statusMessage = 'Your profile unconfirm';
      }
    },
      err => {
        (err.status == 400) ? this.authLogin = "No user found." : this.authLogin = "Incorrect password";
      });
  }

}
