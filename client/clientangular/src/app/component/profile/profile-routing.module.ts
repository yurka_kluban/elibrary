import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import { ShowBookDetailsComponent } from './profile-home/show-book-details/show-book-details.component';
import { ShowAuthorComponent } from './profile-home/show-author/show-author.component';



const routes: Routes = [{
    path: '', component: ProfileComponent,
    children: [
        { path: '', component: ProfileHomeComponent },
        {
            path: 'change-privacy-setting',
            loadChildren: './change-privacy-setting/change-privacy-setting.module#ChangePrivacySettingModule'
        },
        { path: 'show-book-details/:id', component: ShowBookDetailsComponent },
        { path: 'show-author/:id', component: ShowAuthorComponent }
    ]
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule { }