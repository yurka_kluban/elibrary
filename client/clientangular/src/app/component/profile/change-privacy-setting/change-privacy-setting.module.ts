import { NgModule } from '@angular/core';
import { ChangePrivacySettingRoutingModule } from './change-privacy-setting-routing.module';
import { ChangePrivacySettingComponent } from './change-privacy-setting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule ({
    imports: [ChangePrivacySettingRoutingModule,
        FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ],
    declarations: [ChangePrivacySettingComponent]
})

export class ChangePrivacySettingModule { }