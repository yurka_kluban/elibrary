import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { ChangePrivacySettingComponent } from './change-privacy-setting.component';

const routes: Routes = [
    { path: '', component: ChangePrivacySettingComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class ChangePrivacySettingRoutingModule { }