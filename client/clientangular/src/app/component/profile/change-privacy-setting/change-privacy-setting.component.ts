import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'change-privacy-setting',
  templateUrl: './change-privacy-setting.component.html',
  styleUrls: ['./change-privacy-setting.component.scss']
})
export class ChangePrivacySettingComponent implements OnInit {

  public statusMessage: string = '';
  public id: string = '';
  public user: Array<IUserModel> = [];
  public userInfo: any;



  public changeForm: FormGroup = new FormGroup({
    fullName: new FormControl(),
    email: new FormControl(),
    password: new FormControl()
  })


  constructor(private activateRoute: ActivatedRoute,
    private _userService: UserService,
    private router: Router) { }

  public ngOnInit() {

    this.getUser();
  }

  public change(): void {
    this._userService.update(this.userInfo.id , this.changeForm.value).subscribe(res => {
      err => this.statusMessage = 'Error!!!';
    })
  }

  public getUser() {
    this._userService.getUser().subscribe((data: any) => {
      this.userInfo = data;
    })
  }

  public checkConfirm() {
    if (this.userInfo.confirm === false) {
      this.router.navigate(['/home']);
    }

  }

}



