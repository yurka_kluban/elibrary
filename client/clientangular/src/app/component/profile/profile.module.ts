import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile.component';
import { ProfileHomeComponent } from './profile-home/profile-home.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileNavbarComponent } from 'src/app/container/profile-navbar/profile-navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ShowBookDetailsComponent } from './profile-home/show-book-details/show-book-details.component';
import { ShowAuthorComponent } from './profile-home/show-author/show-author.component';
import { NgxPaginationModule } from 'ngx-pagination';





@NgModule({
    declarations: [
        ProfileComponent,
        ProfileHomeComponent,
        ProfileNavbarComponent,
        ShowBookDetailsComponent,
        ShowAuthorComponent
    ],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule
    ],
    providers: [
    ],
})
export class ProfileModule { }