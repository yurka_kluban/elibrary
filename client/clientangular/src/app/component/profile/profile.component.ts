import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { LocalStorageService } from 'src/app/shared/services/localStorage.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private _localStorage: LocalStorageService,
    private router: Router) { }

  ngOnInit() {
    
  }

  public onLogout(): void {
    this._localStorage.deleteToken();
    this.router.navigate(['/home']);
  }

  
}
