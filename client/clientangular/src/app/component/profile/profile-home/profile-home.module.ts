import { NgModule } from '@angular/core';
import { ProfileHomeRoutingModule } from './profile-home-routing.module';
import { ProfileHomeComponent } from './profile-home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule ({
    imports: [
    ProfileHomeRoutingModule,
    FormsModule,
    ReactiveFormsModule
    ],
    declarations: [ProfileHomeComponent]
})

export class ProfileHomeModule { }