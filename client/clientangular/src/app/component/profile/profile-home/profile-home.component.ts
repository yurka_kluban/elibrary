import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/shared/services/book.service';
import { AuthorService } from 'src/app/shared/services/author.service';
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../../shared/models/author.model';
import { IUserModel } from '../../../../../../../shared/models/user.model';



@Component({
  selector: 'app-profile-home',
  templateUrl: './profile-home.component.html',
  styleUrls: ['./profile-home.component.scss']
})
export class ProfileHomeComponent implements OnInit {

  public books: Array<IBookModel> = [];
  public author: Array<IAuthorModel> = [];
  public lengthBooksArray: number;
  public currentPage = 1;
  public itemsPerPage = 1;
  public searchValue: string = '';
  public timer: any;
  public id: string = '';
  public user: Array<IUserModel> = [];

  constructor(private _bookService: BookService,
    private _authorService: AuthorService,) { }

  ngOnInit() {
    this.getBooks(),
    this.getAuthor();
  }

  public getAuthor(): void {
    this._authorService.getAllAuthor().subscribe(data => {
      this.author = data as IAuthorModel[];
    })
  }

  public getBooks() {
    this._bookService.getAllForPagination(this.itemsPerPage, this.currentPage, this.searchValue).subscribe((res: any) => {
      this.books = res.books;
      if (res.length.length < 1) {
        return;
      }
      this.lengthBooksArray = res.length[0].length;
    })
  }

  public getAllBooksForPaging(): void {
    this.lengthBooksArray = 0;
    if (this.timer) {
      clearTimeout(this.timer);
        this.timer = null;
    }
    this.timer =  setTimeout(() => {
      this.getBooks();
    }, 500);
  }

}
