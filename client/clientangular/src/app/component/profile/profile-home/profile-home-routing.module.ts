import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { ProfileHomeComponent } from './profile-home.component';



const routes: Routes = [
    { path: '', component: ProfileHomeComponent },

];

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class ProfileHomeRoutingModule { }