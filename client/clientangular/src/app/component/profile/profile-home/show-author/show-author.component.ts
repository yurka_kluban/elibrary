import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { AuthorService } from 'src/app/shared/services/author.service';
import { BookService } from 'src/app/shared/services/book.service';
import { IBookModel } from '../../../../../../../../shared/models/book.model';


@Component({
  selector: 'show-author',
  templateUrl: './show-author.component.html',
  styleUrls: ['./show-author.component.scss']
})
export class ShowAuthorComponent implements OnInit {

  private id: string = '';
  public author: Array<IAuthorModel> = [];
  public books: Array<IBookModel> = [];

  constructor(
    private activateRoute: ActivatedRoute,
    private _authorService: AuthorService,
    private _bookService: BookService) { 
  }

  ngOnInit() {
    this.getPageId();
    this.getAuthorById();
    this.getBooksByAuthorId();
  }

  public getPageId(): void {
    this.activateRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  public getAuthorById(): void {
    this._authorService.getAuthor(this.id).subscribe(data => {
      this.author = data as IAuthorModel[];
    })
  }

  public getBooksByAuthorId() {
    this._bookService.getByAuthorId(this.id).subscribe(data => {
      this.books = data as IBookModel[]
    })
  }

}