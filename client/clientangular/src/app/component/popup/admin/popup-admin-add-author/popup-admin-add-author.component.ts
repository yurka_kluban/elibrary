import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthorService } from 'src/app/shared/services/author.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';


@Component({
  selector: 'app-popup-admin-add-author',
  templateUrl: './popup-admin-add-author.component.html',
  styleUrls: ['./popup-admin-add-author.component.scss']
})
export class PopupAdminAddAuthorComponent implements OnInit {

  @Output() closePopupAuthorEvt = new EventEmitter()

  public submitType: string = 'Save';
  public statusMessage: string = '';
  public addNewAuthor: Boolean = false;
  public author: Array<IAuthorModel> = [];

  public addAuthor: FormGroup = new FormGroup({
    name: new FormControl()
  })

  constructor(private _authorService: AuthorService) { }

  ngOnInit() {
  }

  public save(): void {
    this._authorService.add(this.addAuthor.value).subscribe(res =>
      this.statusMessage = 'New author created',
      err => this.statusMessage = 'Error!!! Try again, please');
    this.submitType = 'Save';
    this.closePopup();
  }

  public closePopup() {
    this.closePopupAuthorEvt.emit(false);
  }

  public getAuthor(): void {
    this._authorService.getAllAuthor().subscribe(data => {
      this.author = data as IAuthorModel[];
    })
  }

}
