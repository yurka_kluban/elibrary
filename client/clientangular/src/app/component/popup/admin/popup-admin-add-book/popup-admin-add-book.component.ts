import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BookService } from 'src/app/shared/services/book.service';
import { IJournalModel } from '../../../../../../../../shared/models/journal.model';
import { AuthorService } from 'src/app/shared/services/author.service';
import { IAuthorModel } from "../../../../../../../../shared/models/author.model";
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { JournalService } from 'src/app/shared/services/journal.service';



@Component({
  selector: 'app-popup-admin-add-book',
  templateUrl: './popup-admin-add-book.component.html',
  styleUrls: ['./popup-admin-add-book.component.scss']
})
export class PopupAdminAddBookComponent implements OnInit {

  @Output() closePopupBookrEvt = new EventEmitter()

  public submitType: string = 'Save';
  public books: Array<IBookModel> = [];
  public journals: Array<IJournalModel> = [];
  public author: Array<IAuthorModel> = [];
  public selectedPeople: string[];
  public selectedJournal: string[];
  public statusMessage: string = '';
  public imgBase: string = '';


  public addBook: FormGroup = new FormGroup({
    _id: new FormControl(),
    title: new FormControl(),
    image: new FormControl(),
    language: new FormControl(),
    published: new FormControl(),
    pages: new FormControl(),
    author: new FormControl(),
    journal: new FormControl()
  })

  constructor(private _bookService: BookService,
    private _authorService: AuthorService,
    private _journalService: JournalService) { }

  ngOnInit() {
    this.getAuthor();
    this.getJournal();
  }

  public closePopup() {
    this.closePopupBookrEvt.emit(false);
  }

  public getAuthor(): void {
    this._authorService.getAllAuthor().subscribe(data => {
      this.author = data as IAuthorModel[];

    })
  }

  public getJournal(): void {
    this._journalService.getAll().subscribe(data => {
      this.journals = data as IJournalModel[];
    })
  }

  public save(): void {
    let book = {
      title: this.addBook.value.title,
      image: this.imgBase,
      language: this.addBook.value.language,
      published: this.addBook.value.published,
      pages: this.addBook.value.pages,
      author: this.selectedPeople,
      journal: this.selectedJournal
    }
    this._bookService.add(book).subscribe(res =>
      this.statusMessage = 'New book created',
      err => this.statusMessage = 'Error!!! Try again, please');
    this.submitType = 'Save';
    this.closePopup();
  }

  public handleFile(event) {
    let file = event.target.files[0];
    let uploadImg = (image: string) => {
      this.imgBase = image;
    }
    let myReader = new FileReader();
    myReader.onloadend = function () {
      uploadImg(myReader.result.toString());
    };
    myReader.readAsDataURL(file);
  }

  public clearModel() {
    this.selectedPeople = [];
    this.selectedJournal = [];
  }

}
