import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { JournalService } from 'src/app/shared/services/journal.service';

@Component({
  selector: 'app-popup-admin-add-journal',
  templateUrl: './popup-admin-add-journal.component.html',
  styleUrls: ['./popup-admin-add-journal.component.scss']
})
export class PopupAdminAddJournalComponent implements OnInit {

  @Output() closePopupJournalEvt = new EventEmitter()

  public addNewJournal: Boolean = false;
  public status: string = '';
  public submitType: string = 'Save';

  public addJournal: FormGroup = new FormGroup({
    title: new FormControl()
  })

  constructor(private _journalService: JournalService) { }

  ngOnInit() {
  }

  public save(): void {
    this._journalService.add(this.addJournal.value).subscribe(res =>
      this.status = 'New author created',
      err => this.status = 'Error!!! Try again, please');
    this.submitType = 'Save';
    this.closePopup();
  }
  public onAddJournalOpen(): void {
    this.addNewJournal = !this.addNewJournal;
  }

  public closePopup() {
    this.closePopupJournalEvt.emit(false);
  }

}
