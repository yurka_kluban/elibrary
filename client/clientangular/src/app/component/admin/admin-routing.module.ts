import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../admin/admin.component'
import { AdminHomeComponent } from './admin-home/admin-home.component';



const routes: Routes = [{
    path: '', component: AdminComponent,
    children: [
        {path: '', component: AdminHomeComponent},
        {
            path: 'admin-books', 
            loadChildren: './admin-books/admin-books.module#AdminBooksModule'
        },
        {
            path: 'admin-userlist', 
            loadChildren: './admin-userlist/admin-userlist.module#AdminUserlistModule'
        },
        {
            path: 'admin-authors',
            loadChildren: './admin-authors/admin-authors.module#AdminAuthorsModule'
        },
        {
            path: 'admin-journals', 
            loadChildren: './admin-journals/admin-journals.module#AdminJournalsModule'
        },
        ]
    }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }