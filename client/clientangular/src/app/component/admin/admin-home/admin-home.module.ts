import { NgModule } from '@angular/core';
import { AdminHomeRoutingModule } from './admin-home-routing.module';
import { AdminHomeComponent } from './admin-home.component';


@NgModule ({
    imports: [AdminHomeRoutingModule],
    declarations: [AdminHomeComponent]
})

export class AdminHomeModule { }