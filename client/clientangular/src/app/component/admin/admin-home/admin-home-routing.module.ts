import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { AdminHomeComponent } from './admin-home.component';

const routes: Routes = [
    { path: '', component: AdminHomeComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminHomeRoutingModule { }