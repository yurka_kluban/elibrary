import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/shared/services/admin.service';

@Component({
  selector: 'admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  public users: any;
  public statusMessage: String = '';

  constructor(private _adminService: AdminService) { }

  ngOnInit() {
    this.loadUsers();
  }

  public loadUsers(): void {
    this._adminService.getUnconfirmUsers().subscribe(data => {
      this.users = data;
    })
  }

  public confirm(id: string): void {
    this._adminService.confirm(id).subscribe(data => {
      this.loadUsers();
    })
  }

  public reject(event: { target: { id: string | number; }; }): void {
    let idUser = this.users[event.target.id]._id;
    this._adminService.delete(idUser).subscribe(res => {
      this.statusMessage = 'Data successfully deleted';
      this.loadUsers();
    })
  };
}
