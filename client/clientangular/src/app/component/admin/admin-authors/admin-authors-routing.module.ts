import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthorsComponent } from './admin-authors.component';

const routes: Routes = [
    { path: '', component: AdminAuthorsComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminAuthorsRoutingModule { }