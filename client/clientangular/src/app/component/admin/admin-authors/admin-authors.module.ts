import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdminAuthorsRoutingModule } from './admin-authors-routing.module';
import { AdminAuthorsComponent } from './admin-authors.component';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule ({
    imports: [
        AdminAuthorsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgxPaginationModule
    ],
    declarations: [
        AdminAuthorsComponent,
    ]
})

export class AdminAuthorsModule { }