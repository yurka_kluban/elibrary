import { Component, OnInit } from '@angular/core';
import { AuthorService } from 'src/app/shared/services/author.service';
import { IAuthorModel } from "../../../../../../../shared/models/author.model";
import { FormGroup, FormControl } from '@angular/forms';
import { DialogModalComponent } from '../admin-books/dialog-modal/dialog-modal.component';
import { MatDialogConfig, MatDialog } from '@angular/material';

@Component({
  selector: 'admin-authors',
  templateUrl: './admin-authors.component.html',
  styleUrls: ['./admin-authors.component.scss']
})
export class AdminAuthorsComponent implements OnInit {

  public authors: Array<IAuthorModel> = [];
  public author: IAuthorModel;
  public showEdit: boolean = false;
  public statusMessage: string = '';
  public authorId: string = '';
  public lengthAuthorsArray: number;
  public currentPage = 1;
  public itemsPerPage = 1;
  public timer: any;
  public searchValue: string = '';

  public editForm: FormGroup = new FormGroup({
    _id: new FormControl(),
    name: new FormControl()
  })


  constructor(private _authorService: AuthorService,public dialog: MatDialog) { }

  ngOnInit() {
    this.getAuthors();
  }

  public getAuthors(): void {
    this._authorService.getAllForPagination(this.itemsPerPage, this.currentPage, this.searchValue).subscribe((data: any) => {
      this.authors = data.authors;
      if (data.length.length < 1) {
        return;
      }
      this.lengthAuthorsArray = data.length[0].length;
    })
  }

  public getAllAuthorsForPaging(): void {
    this.lengthAuthorsArray = 0;
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
    this.timer = setTimeout(() => {
      this.getAuthors();
    }, 500);
  }

  public edit(author: IAuthorModel): void {
    this.author = author;

    this.showEdit = true;
  }

  public update(author): void {
    this._authorService.update(author._id, this.editForm.value.name).subscribe(res => {

      this.getAuthors();
    },
      err => {
        this.statusMessage = 'Error!!!';
      })
  }

  public delete(_id: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'You are trying to delete an author!'
    };
    const dialogRef = this.dialog.open(DialogModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._authorService.delete(_id).subscribe(res => {
          this.statusMessage = 'Data successfully deleted';
          this.getAllAuthorsForPaging();
        });
      }
    });
  }

  public cancelEdit(): void {
    this.showEdit = false;
  }

}
