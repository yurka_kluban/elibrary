import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdminJournalsComponent } from './admin-journals.component';
import { AdminJournalsRoutingModule } from './admin-journals-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule ({
    imports: [
        AdminJournalsRoutingModule,
        FormsModule,
        ReactiveFormsModule,    
        CommonModule,
        NgxPaginationModule
    ],
    declarations: [
        AdminJournalsComponent
    ]
})

export class AdminJournalsModule { }