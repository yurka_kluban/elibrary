import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { AdminJournalsComponent } from './admin-journals.component';


const routes: Routes = [
    { path: '', component: AdminJournalsComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminJournalsRoutingModule { }