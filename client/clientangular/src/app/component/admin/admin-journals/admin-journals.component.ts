import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl } from '@angular/forms';
import { IJournalModel } from '../../../../../../../shared/models/journal.model';
import { JournalService } from 'src/app/shared/services/journal.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DialogModalComponent } from '../admin-books/dialog-modal/dialog-modal.component';

@Component({
  selector: 'admin-journals',
  templateUrl: './admin-journals.component.html',
  styleUrls: ['./admin-journals.component.scss']
})
export class AdminJournalsComponent implements OnInit {

  public journals: Array<IJournalModel> = [];
  public journal: IJournalModel;
  public showEdit: boolean = false;
  public statusMessage: string = '';
  public journalId: string = '';
  public lengthJournalsArray: number;
  public currentPage = 1;
  public itemsPerPage = 1;
  public searchValue: string = '';
  public timer: any;

  public editForm: FormGroup = new FormGroup({
    id: new FormControl(),
    title: new FormControl()
  })

  constructor(
    private _journalService: JournalService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getJournals();
  }

  public getJournals(): void {
    this._journalService.getAllForPagination(this.itemsPerPage, this.currentPage, this.searchValue).subscribe((data: any) => {
      this.journals = data.journals;
      if (data.length.length < 1) {
        return;
      }
      this.lengthJournalsArray = data.length[0].length;
    })
  }

  public getAllJournalsForPaging(): void {
    this.lengthJournalsArray = 0;
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
    this.timer = setTimeout(() => {
      this.getJournals();
    }, 500);
  }

  public edit(journal): void {
    this.journal = journal;
    this.showEdit = true;
  }

  public update(journal): void {
    this._journalService.update(journal._id, this.editForm.value.title).subscribe(res => {
      this.getJournals();
    },
      err => {
        this.statusMessage = 'Error!!!';
      })
    this.showEdit = false;
  }

  public delete(id: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'You are trying to delete an journal!'
    };
    const dialogRef = this.dialog.open(DialogModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._journalService.delete(id).subscribe(res => {
          this.statusMessage = 'Data successfully deleted';
          this.getAllJournalsForPaging();
        });
      }
    });
  }

  public cancelEdit(): void {
    this.showEdit = false;
  }

}
