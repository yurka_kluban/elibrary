import { NgModule } from '@angular/core';
import { AdminBooksRoutingModule } from './admin-books-routing.module';
import { AdminBooksComponent } from './admin-books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PopupAdminAddJournalComponent } from '../../popup/admin/popup-admin-add-journal/popup-admin-add-journal.component';
import { PopupAdminAddBookComponent } from '../../popup/admin/popup-admin-add-book/popup-admin-add-book.component';
import { PopupAdminAddAuthorComponent } from '../../popup/admin/popup-admin-add-author/popup-admin-add-author.component';
import { AdminBooksEditComponent } from './admin-books-edit/admin-books-edit.component';
import { AdminShowBookComponent } from './admin-show-book/admin-show-book.component';
import { AdminShowAuthorComponent } from './admin-show-author/admin-show-author.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';



@NgModule({
    imports: [AdminBooksRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgxPaginationModule,
        NgSelectModule
    ],
    declarations: [
        AdminBooksComponent,
        PopupAdminAddJournalComponent,
        PopupAdminAddBookComponent,
        PopupAdminAddAuthorComponent,
        AdminBooksEditComponent,
        AdminShowBookComponent,
        AdminShowAuthorComponent,
    ]
    
})

export class AdminBooksModule { }