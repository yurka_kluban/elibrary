import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { AdminBooksComponent } from './admin-books.component';
import { AdminBooksEditComponent } from './admin-books-edit/admin-books-edit.component';
import { AdminShowBookComponent } from './admin-show-book/admin-show-book.component';
import { AdminShowAuthorComponent } from './admin-show-author/admin-show-author.component';


const routes: Routes = [
    { path: '', component: AdminBooksComponent },
    { path: 'admin-books-edit/:id', component: AdminBooksEditComponent },
    { path: 'admin-show-book/:id', component: AdminShowBookComponent },
    { path: 'admin-show-author/:id', component: AdminShowAuthorComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminBooksRoutingModule { }