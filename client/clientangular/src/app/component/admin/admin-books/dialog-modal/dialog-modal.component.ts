import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-modal',
  templateUrl: './dialog-modal.component.html',
  styleUrls: ['./dialog-modal.component.scss']
})
export class DialogModalComponent implements OnInit {
  
  public modalTitle: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.modalTitle = data.message;
  }


  ngOnInit() {
  }

}
