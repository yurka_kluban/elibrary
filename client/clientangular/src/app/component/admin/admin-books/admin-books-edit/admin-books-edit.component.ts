import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from 'src/app/shared/services/book.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IJournalModel } from '../../../../../../../../shared/models/journal.model';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { AuthorService } from 'src/app/shared/services/author.service';
import { JournalService } from 'src/app/shared/services/journal.service';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { IUserModel } from '../../../../../../../../shared/models';



@Component({
  selector: 'admin-books-edit',
  templateUrl: './admin-books-edit.component.html',
  styleUrls: ['./admin-books-edit.component.scss']
})

export class AdminBooksEditComponent implements OnInit {

  public imgBase: string = '';
  public bookId: string = '';
  public authors: Array<IAuthorModel> = [];
  public book: any = [];
  public journals: Array<IJournalModel> = [];


  public editBook: any = new FormGroup({
    title: new FormControl(),
    image: new FormControl(),
    language: new FormControl(),
    published: new FormControl(),
    pages: new FormControl(),
    author: new FormControl(),
    journal: new FormControl()
  })

  constructor(
    private _bookService: BookService,
    private _authorService: AuthorService,
    private _journalService: JournalService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }


  ngOnInit() {
    this.getBookId();
    this.getBookById();
    this.getJournal();
    this.getAuthor();
  }

  public getBookId() {
    this.activatedRoute.params.subscribe(res => {
      this.bookId = res['id'];
    })
  }

  public getBookById(): void {
    this._bookService.getById(this.bookId).subscribe((res: any) => {
      res.author = res.author as Array<IUserModel>;
      this.book = res;
      this.book.author = [].map.call(this.book.author, item =>{
        return item._id;
      })
    })
  }

  public getJournal(): void {
    this._journalService.getAll().subscribe(data => {
      this.journals = data as IJournalModel[];
    });
  }

  public getAuthor(): void {
    this._authorService.getAllAuthor().subscribe(data => {
      this.authors = data as IAuthorModel[];
    });
  }

  public handleFile(event) {
    let file = event.target.files[0];
    let uploadImg = (image: string) => {
      this.imgBase = image;
    }
    let myReader = new FileReader();
    myReader.onloadend = function () {
      uploadImg(myReader.result.toString());
    };
    myReader.readAsDataURL(file);
  }

  public update() {

    this.editBook.value.image = this.imgBase;
    this._bookService.update(this.bookId, this.editBook.value).subscribe((result) => {
      this.router.navigate(['/admin/admin-books/']);
    }, (err) => {
    });
  }
}