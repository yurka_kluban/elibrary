import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/shared/services/book.service';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';



@Component({
  selector: 'admin-show-book',
  templateUrl: './admin-show-book.component.html',
  styleUrls: ['./admin-show-book.component.scss']
})
export class AdminShowBookComponent implements OnInit {

  private id: string= '';
  public book: Array<IBookModel> = [];
  public author: Array<IAuthorModel> = [];

  constructor(
    private activateRoute: ActivatedRoute,
    private _bookService: BookService,) { }

  ngOnInit() {
    this.getPageId();
    this.getBook();
  }

  public getPageId(): void {
    this.activateRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  public getBook(): void {
    this._bookService.getById(this.id).subscribe(data => {
      this.book = data as IBookModel[];
    })
  }

}
