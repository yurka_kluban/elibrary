import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/shared/services/book.service';
import { AuthorService } from 'src/app/shared/services/author.service';
import { IJournalModel } from '../../../../../../../shared/models/journal.model';
import { IAuthorModel } from "../../../../../../../shared/models/author.model";
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { JournalService } from 'src/app/shared/services/journal.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DialogModalComponent } from './dialog-modal/dialog-modal.component';


@Component({
  selector: 'admin-books',
  templateUrl: './admin-books.component.html',
  styleUrls: ['./admin-books.component.scss']
})
export class AdminBooksComponent implements OnInit {


  public addBook: Boolean = false;
  public addAuthor: Boolean = false;
  public addJournal: Boolean = false;
  public books: Array<IBookModel> = [];
  public author: Array<IAuthorModel> = [];
  public journals: Array<IJournalModel> = [];
  public lengthBooksArray: number;
  public currentPage = 1;
  public itemsPerPage = 1;
  public searchValue: string = '';
  public timer: any;



  constructor(private _bookService: BookService,
    private _authorService: AuthorService,
    private _journalService: JournalService,
    public dialog: MatDialog) { }


  ngOnInit() {
    this.getBooks();
    this.getAuthor();
    this.getJournal();
  }

  public getAuthor(): void {
    this._authorService.getAllAuthor().subscribe(data => {
      this.author = data as IAuthorModel[];
    })
  }

  public getJournal(): void {
    this._journalService.getAll().subscribe(data => {
      this.journals = data as IJournalModel[];
    })
  }

  public getBooks(): void {
    this._bookService.getAllForPagination(this.itemsPerPage, this.currentPage, this.searchValue).subscribe((res: any) => {
      this.books = res.books;
      if (res.length.length < 1) {
        return;
      }
      this.lengthBooksArray = res.length[0].length;
    });
  }

  public getAllBooksForPaging(): void {
    this.lengthBooksArray = 0;
    if (this.timer) {
      clearTimeout(this.timer);
        this.timer = null;
    }
    this.timer =  setTimeout(() => {
      this.getBooks();
    }, 500);
  }
  
  public delete(id: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'You are trying to delete a book!'
    };
    const dialogRef = this.dialog.open(DialogModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this._bookService.delete(id).subscribe(data => {
          this.getAllBooksForPaging();
        }, (err) => {
        });
      }
    }); 
  }

  public showPopupBook(): void {
    this.addBook = !this.addBook;
    this.addJournal = false;
    this.addAuthor = false;
  }

  public closePopupBook(event) : void {
    this.addBook = event;
    this.getAllBooksForPaging();
  }

  public showPopupAuthor(): void {
    this.addAuthor = !this.addAuthor;
    this.addJournal = false;
    this.addBook = false;
  }

  public closePopupAuthor(event) : void {
    this.addAuthor = event;
    this.getAllBooksForPaging();
  }

  public showPopupJournal(): void {
    this.addJournal = !this.addJournal;
    this.addBook = false;
    this.addAuthor = false;
  }

  public closePopupJournal(event) : void {
    this.addJournal = event;
    this.getAllBooksForPaging();
  }

}
