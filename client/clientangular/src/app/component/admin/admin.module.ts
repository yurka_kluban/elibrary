import { AdminComponent } from './admin.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminRoutingModule } from './admin-routing.module'
import { NgModule } from '@angular/core';
import { AdminNavbarComponent } from 'src/app/container/admin-navbar/admin-navbar.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { DialogModalComponent } from '../admin/admin-books/dialog-modal/dialog-modal.component';
import { MatDialogModule, MatButtonModule } from '@angular/material';



@NgModule({
    declarations: [
        AdminComponent,
        AdminNavbarComponent,
        AdminHomeComponent,
        DialogModalComponent
    ],
    imports: [
        AdminRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        MatDialogModule,
        MatButtonModule
    ],
    providers: [
    ],
    
     

})
export class AdminModule { }