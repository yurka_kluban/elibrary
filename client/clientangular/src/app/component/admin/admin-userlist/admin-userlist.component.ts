import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AdminService } from 'src/app/shared/services/admin.service';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DialogModalComponent } from '../admin-books/dialog-modal/dialog-modal.component';


@Component({
  selector: 'admin-userlist',
  templateUrl: './admin-userlist.component.html',
  styleUrls: ['./admin-userlist.component.scss']
})
export class AdminUserlistComponent implements OnInit {

  public showNew: boolean = false;
  public showEdit: boolean = false;
  public user: IUserModel;
  public users: any = [];
  public statusMessage: string = '';
  public lengthUsersArray: number;
  public currentPage = 1;
  public itemsPerPage = 2;
  public searchValue: string = '';
  public timer: any;
  public adminInfo: any;


  public addNewUserForm: FormGroup = new FormGroup({
    id: new FormControl(),
    fullName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    confirm: new FormControl(true),
    role: new FormControl()
  })

  constructor(
    private _adminService: AdminService,
    public dialog: MatDialog   
  ) { }

  ngOnInit() {
    this.getUsers();
    this.getPageId();
  }

  
  public getPageId() {
    this._adminService.get().subscribe((data: any) => {
      this.adminInfo = data;
    });
  }

  public getUsers(): void {
    this._adminService.getAllForPagination(this.itemsPerPage, this.currentPage, this.searchValue).subscribe((data: any) => {
      this.users = data.users;
      if (data.length.length < 1) {
        return;
      }
      this.lengthUsersArray = data.length[0].length;
    });
  }

  public getAllJournalsForPaging(): void {
    this.lengthUsersArray = 0;
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
    this.timer = setTimeout(() => {
      this.getUsers();
    }, 500);
  }

  public save(event: { preventDefault: () => void; }): void {
    event.preventDefault();
    this.addNewUserForm.value.role = parseInt(this.addNewUserForm.value.role);
    this._adminService.add(this.addNewUserForm.value).subscribe(res =>
      this.statusMessage = 'New user created',
      err => this.statusMessage = 'Error!!! Try again, please');
    this.getAllJournalsForPaging();
    this.showNew = false;
  }

  public update(user): void {
    this.addNewUserForm.value.role = +this.addNewUserForm.value.role;
    this._adminService.update(user._id, this.addNewUserForm.value).subscribe(res => {
      this.getUsers();
    },
      err => {
        this.statusMessage = 'Error!!!';
      })
    this.showEdit = !this.showEdit;
  }

  public ban(id: string): void {
    this._adminService.ban(id).subscribe(data => {
      this.getAllJournalsForPaging();
    })
  }

  public delete(id: string): void {
    if (this.adminInfo.id == id){
      return;
    }
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'You are trying to delete an journal!'
    };
    const dialogRef = this.dialog.open(DialogModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._adminService.delete(id).subscribe(res => {
          this.statusMessage = 'Data successfully deleted';
          this.getAllJournalsForPaging();
        })
      }
    })
  }

  public edit(user): void {
    this.user = user;
    this.showEdit = !this.showEdit;
  }

  public showPopup() {
    this.showNew = !this.showNew;
  }
}




