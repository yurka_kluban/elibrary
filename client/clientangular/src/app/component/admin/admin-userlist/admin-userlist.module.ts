import { NgModule } from '@angular/core';
import { AdminUserlistRoutingModule } from './admin-userlist-routing.component';
import { AdminUserlistComponent } from './admin-userlist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';




@NgModule ({
    imports: [
        AdminUserlistRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgxPaginationModule
    ],
    declarations: [
        AdminUserlistComponent,
    ]
})

export class AdminUserlistModule { }