import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router';
import { AdminUserlistComponent } from './admin-userlist.component';

const routes: Routes = [
    { path: '', component: AdminUserlistComponent }
]

@NgModule ({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class AdminUserlistRoutingModule { }