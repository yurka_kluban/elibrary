import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/services/localStorage.service';


@Component({
  selector: 'profile-navbar',
  templateUrl: './profile-navbar.component.html',
  styleUrls: ['./profile-navbar.component.scss']
})
export class ProfileNavbarComponent implements OnInit {

  constructor(private _localStorage: LocalStorageService) { }

  ngOnInit() {
  }

  logout () {
    this._localStorage.deleteToken();
  }


}
