import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/services/localStorage.service';

@Component({
  selector: 'admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss']
})
export class AdminNavbarComponent implements OnInit {

  constructor(private _localStorage: LocalStorageService) { }

  ngOnInit() {
  }

  logout () {
    this._localStorage.deleteToken();
  }

}
