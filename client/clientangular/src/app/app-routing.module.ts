import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './component/auth/home/home.component';
import { AuthGuard } from './shared/guadrs/auth.guard';



const routes: Routes = [
  {path: 'auth', component: HomeComponent},
  {
    path: 'auth/register',
    loadChildren: './component/auth/register/register.module#RegisterModule',

  },
  {
    path: 'auth/login',
    loadChildren: './component/auth/login/login.module#LoginModule',

  },
  {
    path: 'profile',
    loadChildren: './component/profile/profile.module#ProfileModule',
    canActivate: [AuthGuard],
    data: {roles: ['1']}
  },
  {
    path: 'admin',
    loadChildren: './component/admin/admin.module#AdminModule',
    canActivate: [AuthGuard],
    data: {roles: ['2']}
  },
  {path: '**', redirectTo: 'auth'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
