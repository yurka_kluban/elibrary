export interface IBookModel {
    title?: string,
    image?: any;
    language?: string,
    published?: number,
    pages?: number,
    author?: String[],
    journal?: String[]
}