import { UserRole } from "../models/index";


export interface IUserModel {
    _id:any;
    email: string,
    fullName: string,
    password: string,
    confirm: Boolean,
    role: UserRole
}
