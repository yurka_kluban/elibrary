import { Request, Response} from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AuthController } from "../controllers/authController"
import { AdminController } from "../controllers/adminController"
import { UserController } from "../controllers/userController"
import { UserRole } from "../../../shared/models/index";
import { JournalController } from "../controllers/journalController";

export class Routes {

    public bookController: BookController = new BookController()
    public authorController: AuthorController = new AuthorController()
    public authController: AuthController = new AuthController()
    public adminController: AdminController = new AdminController()
    public userController: UserController = new UserController()
    public journalController: JournalController = new JournalController()

    public routes(app): void {

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                })
            })

        app.route('/test')
            .get((req: Request, res: Response) => {
            res.status(200).send({
                message: 'GET request successfulll!!!!'
            })
        })
            .post((req: Request, res: Response) => {
            res.status(200).send({
                message: 'POST request successfulll!!!!'
            })
        })

        // Auth
        app.route('/auth/profile')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authController.profile);
        app.route('/auth/register')
            .post(this.authController.register);
        app.route('/auth/login')
            .post(this.authController.login);


        //Profile
        app.route('/profile')
            .get(AuthMiddleware([UserRole.user]), this.userController.get);
        app.route('/profile/:id')
            .get(AuthMiddleware([UserRole.user]), this.userController.getById)
            .put(AuthMiddleware([UserRole.user]), this.userController.update);

        //Admin
        app.route('/admin')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.get)
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.adminController.add);
        app.route('/admin/users/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.adminController.search)    
        app.route('/admin/unconfirmUsers')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getUnconfirmUsers)
        app.route('/admin/confirmOrBan/:id')
            .put(AuthMiddleware([UserRole.admin]), this.adminController.confirmOrBan)
        app.route('/admin/:id')
            .put(AuthMiddleware([UserRole.admin]), this.adminController.update)
            .delete(AuthMiddleware([UserRole.user, UserRole.admin]), this.adminController.delete)

        // Book 
        app.route('/book')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.get)    
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.add);
        app.route('/book/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.bookController.search)
        app.route('/book/author/:authorId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.getByAuthorId)       
        app.route('/book/journal/:journalId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.getByJournalId)
        app.route('/book/:bookId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.getById)
            .put(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.update)
            .delete(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.delete)

        // Author 
        app.route('/author')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.get)
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.add);
        app.route('/authors/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.authorController.search)
        app.route('/author/:id')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.getById)
            .put(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.update)
            .delete(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.delete)

        // Journal 
        app.route('/journal')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.journalController.get)
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.journalController.add);
        app.route('/journals/paging')
            .post(AuthMiddleware([UserRole.admin, UserRole.user]), this.journalController.search)
        app.route('/journal/:journalId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.journalController.getById)
            .put(AuthMiddleware([UserRole.user, UserRole.admin]), this.journalController.update)
            .delete(AuthMiddleware([UserRole.user, UserRole.admin]), this.journalController.delete)

    }
}