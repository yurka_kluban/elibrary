import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { JournalRepository } from '../repositories/journalRepository';



export class JournalController {

    public get(req: RequestModel<{}>, res: Response) {
        JournalRepository.find({}, (err, journal) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(journal);
        });
    }
    
    public getById(req: RequestModel<{ id: number }>, res: Response) {
        JournalRepository.findById(req.params.id, (err, journal) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(journal);
        });
    }
    
    public search(req: RequestModel<{ searchValue: string, lengthJournalsArray: number, currentPage: number }>, res: Response) {
        const search = { $regex: ".*" + req.body.searchValue + ".*", $options: 'i' }
        let length = JournalRepository.aggregate([
            {
                $match:
                {
                    title: search
                }
            }
        ])
        .count('length')
        .exec()
        let journal = JournalRepository.aggregate([
            {
                $match:
                {
                    title: search
                }
            }
        ])
        .skip(req.body.itemsPerPage * (req.body.currentPage - 1))
        .limit(req.body.itemsPerPage)
        .exec()
        Promise.all([length, journal]).then(result => res.json({ length: result[0], journals: result[1] }))
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newJournal = new JournalRepository(req.body);
        newJournal.save((err, journal) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(journal);
        });
    }

    public update(req: RequestModel<{ journalId: any }>, res: Response) {
        JournalRepository.findOneAndUpdate({ _id: req.params.journalId }, req.body, { new: true }, (err, journal) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(journal);
        });
    }

    public delete(req: RequestModel<{ journalId: string }>, res: Response) {
        JournalRepository.updateMany({ journal: req.params.journalId }, { $pull: { 'journal': req.params.journalId } }, (err, journal) => {
            if (err) return res.status(500).send('Error on the server.');
            JournalRepository.remove({ _id: req.params.journalId }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json({ message: 'Successfully deleted contact!' });
            });
        })
    }

}
