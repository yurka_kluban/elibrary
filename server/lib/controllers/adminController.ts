import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';




export class AdminController {

    public get(req: RequestModel<{}>, res: Response) {
        res.status(200).send(req.user);
    }

    public getUnconfirmUsers(req: RequestModel<{}>, res: Response) {
        UserRepository.find({ confirm: false }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.status(200).send(user);
        })
    }

    public search(req: RequestModel<{ searchValue: string, lengthUsersArray: number, currentPage: number }>, res: Response) {
        const search = { $regex: ".*" + req.body.searchValue + ".*", $options: 'i' }
        let length = UserRepository.aggregate([
            {
                $match:
                {
                    confirm: true,
                    fullName: search
                }
            }
        ])
        .count('length')
        .exec()
        let user = UserRepository.aggregate([
            {
                $match:
                {
                    confirm: true,
                    fullName: search
                }
            }
        ])
        .skip(req.body.itemsPerPage * (req.body.currentPage - 1))
        .limit(req.body.itemsPerPage)
        .exec()
        Promise.all([length, user]).then(result => res.json({ length: result[0], users: result[1] }))
    }

    public add(req: RequestPost<{ fullName: string, email: string, password: string, confirm: Boolean, role: number }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        UserRepository.create({
            fullName: req.body.fullName,
            email: req.body.email,
            password: hashedPassword,
            confirm: req.body.confirm,
            role: req.body.role
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                // create a token
                var token = jsonwebtoken.sign({ id: user._id, fullName: user.fullName, email: user.email, confirm: user.confirm, role: user.role }, authConfig.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token, user: user });
            });
    }

    public update(req: RequestModel<{ id: string }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        req.body.password = hashedPassword
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public confirmOrBan(req: RequestModel<{ id: string }>, res: Response) {
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public delete(req: RequestModel<{ id: string }>, res: Response) {
        UserRepository.remove({ _id: req.params.id }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    };

}