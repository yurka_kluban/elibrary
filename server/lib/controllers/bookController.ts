import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { BookRepository } from '../repositories/bookRepository';
import * as mongoose from 'mongoose';


export class BookController {


    public getByAuthorId(req: RequestModel<{ authorId: string }>, res: Response) {
        BookRepository.find({ 'author': req.params.authorId }, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public getByJournalId(req: RequestModel<{ journalId: string }>, res: Response) {
        let journalId = req.params.journalId;
        BookRepository.find({ journalId: journalId }, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public get(req: RequestModel<{}>, res: Response) {
        BookRepository.find({}, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public search(req: RequestModel<{ searchValue: string, lengthBooksArray: number, currentPage: number }>, res: Response) {
        console.log(res)
        const search = { $regex: ".*" + req.body.searchValue + ".*", $options: 'i' }
        const length = BookRepository.aggregate([
            {
                "$lookup":
                {
                    from: 'authors',
                    localField: 'authors',
                    foreignField: '_id',
                    as: 'authors'
                }
            },
            {
                "$match":
                {
                    $or:
                        [{ title: search }, { "authors.name": search }]
                }
            },

        ])
            .count('length')
            .exec()

        let books = BookRepository.aggregate([
            {
                "$lookup":
                {
                    from: 'authors',
                    localField: 'author',
                    foreignField: '_id',
                    as: 'author'
                }
            },
            {
                "$lookup":
                {
                    from: 'journals',
                    localField: 'journal',
                    foreignField: '_id',
                    as: 'journal'
                }
            },
            {
                "$match":
                {
                    $or:
                        [{ title: search }, { "author.name": search }, { "journal.title": search }]
                }
            }
        ])
            .skip(req.body.itemsPerPage * (req.body.currentPage - 1))
            .limit(req.body.itemsPerPage)
            .exec()
        Promise.all([length, books]).then(result => res.json({ length: result[0], books: result[1] }))
    }

    public getById(req: RequestModel<{ bookId: string }>, res: Response) {
        const ObjectId = mongoose.Types.ObjectId;
        BookRepository.aggregate([
            {
                $match:
                {
                    _id: ObjectId(req.params.bookId)
                }
            },
            {
                $lookup:
                {
                    from: 'authors',
                    localField: 'author',
                    foreignField: '_id',
                    as: 'author'
                }
            },
        ]).exec((err, book) => {
            if (err) return res.send("error");
            res.json(book[0]);
        })
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newBook = new BookRepository(req.body);
        newBook.save((err, book) => {
            if (err) return res.status(500).send('Error on the server.')
            res.json(book);
        });
    }

    public update(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.findOneAndUpdate({ _id: req.params.bookId }, req.body, { new: true }, (err, contact) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(contact);
        });
    }

    public delete(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.remove({ _id: req.params.bookId }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}