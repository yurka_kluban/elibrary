import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { AuthorRepository } from '../repositories/authorRepository';
import { BookRepository } from '../repositories/bookRepository';


export class AuthorController {
  
    public get(req: RequestModel<{}>, res: Response) {
        AuthorRepository.find({}, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }
    
    public getById(req: RequestModel<{ id: string }>, res: Response) {
        AuthorRepository.findById(req.params.id, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }
    
    public search(req: RequestModel<{ searchValue: string, lengthAuthorsArray: number, currentPage: number }>, res: Response) {
        const search = { $regex: ".*" + req.body.searchValue + ".*", $options: 'i' }
        let length = AuthorRepository.aggregate([
            {
                $match:
                {
                    name: search
                }
            }
        ])
        .count('length')
        .exec()
        let author = AuthorRepository.aggregate([
            {
                $match:
                {
                    name: search
                }
            }
        ])
        .skip(req.body.itemsPerPage * (req.body.currentPage - 1))
        .limit(req.body.itemsPerPage)
        .exec()
        Promise.all([length, author]).then(result => res.json({ length: result[0], authors: result[1] }))
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newAuthor = new AuthorRepository(req.body);
        newAuthor.save((err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public update(req: RequestModel<{ id: any }>, res: Response) {

        AuthorRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public delete(req: RequestModel<{ id: number }>, res: Response) {
        BookRepository.updateMany({ author: req.params.id }, { $pull: { 'author': req.params.id } }, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            AuthorRepository.remove({ _id: req.params.id }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json({ message: 'Successfully deleted contact!' });
            });
        })
    }

}