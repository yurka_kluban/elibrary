import { RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import { UserRepository } from '../repositories/userRepository';
import * as bcryptjs from 'bcryptjs';
import { IUserModel } from '../../../shared/models';


export class UserController {

    public get (req: RequestModel<IUserModel>, res: Response) {
        UserRepository.find({}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public getById (req: RequestModel<{id: string}>, res: Response) {           
        UserRepository.findById(req.params.id, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public update (req: RequestModel<{id: string}>, res: Response) {  
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        req.body.password = hashedPassword       
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }  
}