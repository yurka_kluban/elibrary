import * as mongoose from 'mongoose';
import { IBookModel } from '../../../shared/models/book.model';


const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId
interface IBookEntity extends IBookModel, mongoose.Document { }
export const BookSchema = new Schema({
    title:  String,
    image: String,
    language: String,
    published: Number,
    pages: Number,
    author: [ObjectId],
    journal:  [ObjectId]
});
export const BookRepository = mongoose.model<IBookEntity>('Book', BookSchema);