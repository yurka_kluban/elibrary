import * as mongoose from 'mongoose';
import { IJournalModel } from '../../../shared/models/journal.model';

const Schema = mongoose.Schema;
interface IJournalEntity extends IJournalModel, mongoose.Document { }
export const JournalSchema = new Schema({
    title: String,
});
export const JournalRepository = mongoose.model<IJournalEntity>('Journal', JournalSchema);